// Programmer : Yash Goenka
// Date : 25/2/2016


package que1.bank;

import java.util.Scanner;


public class Que1Bank {

    
    public static void main(String[] args) {
       
        
        byte ch ;
        byte type;
        boolean t=true;
        Scanner in = new Scanner(System.in) ;
        Account a = null ;
        
        System.out.print("\n1. Savings");
        System.out.print("\n2. Current");
        System.out.print("\nEnter your choice: ");
        type = in.nextByte();
        if (type==2)
            a= new Current("2011502",500,5000);
        if (type==1)
            a= new Savings("2011502",500,2);
        
        while(true)
        {
            System.out.println("1 - Create new account");
            System.out.println("2 - View account number");
            System.out.println("3 - Check balance");
            System.out.println("4 - Withdraw cash");
            System.out.println("5 - Deposit");
            if(a instanceof Savings )
            {
                System.out.println("6 - Show the rate of interest");
                System.out.println("7 - Check the interest you are gaining");
            }
            else
                System.out.println("6 - Show the overdraft limit");
            System.out.println("0 - Exit");
            System.out.println("Enter a choice");
            
            ch = in.nextByte();
            
            switch(ch){
                
                case 1:
                    System.out.println("Please enter the amount to deposit");
                    double balance = in.nextDouble();
                  
                    if(2==type)
                    {
                        
                        
                    System.out.println("Please enter the overdraft limit");
                    float draft = in.nextFloat();t=true;
                    while(t==true)
                    {
                        if(draft>500 && draft<10000000)
                            break;
                        else
                        {
                            System.out.println("Please enter the limit between 500 and 10000000");
                            draft = in.nextFloat();
                        }
                    }
                    a= new Current("2011502",balance,draft);
                    }
                    else
                    {
                        while(true)
                        {
                            if(balance >1000)
                                break;
                            else
                                System.out.println("You cannot create an account with less than 1000 rupees");
                                System.out.println("Please enter the new amount to deposit");
                                 balance = in.nextDouble();
                        }
                        a= new Savings ("2301589",balance,5);
                    }
                    break;
                
                case 2 :
                        
                        String acc =new String();
                        acc=a.getAccno();
                        System.out.println("Account number is " +acc);
                        break;
                case 3 :
                        
                    
                    System.out.println("Your current balance is "+ a.getBal());
                    break;
                case 4 :
                     a.withdraw();
                     break;
                case 5:
                    a.Deposit();
                    break;
                case 6:
                    if(a instanceof Savings)
                        System.out.println("The rate of interest is "+((Savings) a).getRoi()+"%");
                    else
                        System.out.println("The overdraft limit is "+ ((Current)a).getodl());
                    break;
                case 7:
                    System.out.println("After how many quarter(s) you want to check the interest");
                    int q = in.nextInt();
                    ((Savings)a).getinterest(q);
                    break;
                case 0: return;
                    
                    
            }
        }
        
        
    }
    
}
