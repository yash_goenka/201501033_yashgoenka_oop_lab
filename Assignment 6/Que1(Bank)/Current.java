// Programmer : Yash Goenka
// Date : 25/2/2016


package que1.bank;

import java.util.Scanner;


public class Current extends Account
{
    Scanner in = new Scanner(System.in); 
    float odl;
    
    public Current(String no, double bal, float draft) {
        super(no, bal);
        odl = draft; 
        
    }
    
    public void withdraw()
    {
        System.out.println("Please enter the amount to withdraw");
        double amt = in.nextDouble();
        if(amt>odl)
        {
            System.out.println("The entered amount cannot be withfrawn beacause your over draft limit is "+ odl );
        }
        else
        {
            acc_bal-=amt;
            System.out.println("The updated account balance is "+ acc_bal);
        }
    }
    
    public float getodl()
    {
        return odl;
    }
    
}
