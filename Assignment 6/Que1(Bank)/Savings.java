// Programmer : Yash Goenka
// Date : 25/2/2016


package que1.bank;

import java.util.Scanner;


public class Savings extends Account  {
    
    float roi;
    Scanner in = new Scanner(System.in);

    Savings (String no, double bal, float interest)
    {
        super(no,bal);
        roi= interest;
    }
    
    
    @Override
    public void withdraw()
    {
        System.out.println("Enter the amount to withdraw");
        double amt = in.nextDouble();
        double temp = acc_bal -amt;
        if(temp<500)
        {
            System.out.println("The entered amount cannot be withdrawn beacuse of insufficient balance");
        }
        
        else
        {
            acc_bal=temp;
            System.out.println("The transaction was successful");
            System.out.println("The updated balance is "+ acc_bal);
        }
    
    
    
    
}
    
    public float getRoi()
    {
        return roi;
    }
    
    public void getinterest(int q)
    {
        double bal = getBal();
        double interest=0;
        for (int i = 0;i<q;i++)
        {
            interest+=bal*roi/100;
            bal+=interest;
        }
        System.out.println("Your current balance is " + getBal());
        System.out.println("You will get interese of "+ interest +" rupees after "+q+" quarter(s)");
        System.out.println("Your updated balance will be "+bal+" rupees after " +q+" quarter(s)"); 
        
    }
    
}
