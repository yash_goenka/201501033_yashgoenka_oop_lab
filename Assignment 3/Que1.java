import java.util.Scanner;
import static java.lang.Math.pow;

class Number
{
	private float coeff;
	private int degree;
	public void degree()
	{	Scanner input = new Scanner(System.in);
		
			System.out.println("\nEnter Degree of a polynomial: ");
			this.degree = input.nextInt();
	}
	public void coeff (int n)
	{	
		Scanner input = new Scanner(System.in);
		System.out.println("\nEnter the coeff of X^("+n+")");
		this.coeff = input.nextFloat();
	}

	public Float getcoeff1()
	{
		return coeff;
	}
	public void setcoeff(Float f)
	{
		coeff=f;
	}
	public void setdegree(int n)
	{
		degree=n;
	}
	
	public int getdegree()
	{
		return degree;
	}
	

}





class LinkedList
{
	private Number data = null;
	private LinkedList next = null;



	public void append(Number newData)
	{ 
		if(data == null)
			this.data = newData;
		else
		{
			LinkedList curr;
			for(curr = this; curr.next!=null; curr = curr.next);

			curr.next = new LinkedList();
			curr = curr.next;
			curr.data = newData;
			curr.next = null;
		}
	}
	public int count()
	{	int cnt=0;
		LinkedList curr;
		for(curr = this; curr!=null; curr = curr.next)
		cnt++;
		return cnt;
	}
	
	public void display()
	{	int n=count();
	
		if(this.data == null)
			System.out.println("\nList is Empty!");
		else
		{	System.out.println("\nThe polynomial is: ");
			LinkedList curr;
			for(curr = this; curr!=null; curr = curr.next)
			{	if(curr.next!=null)
				System.out.print("("+curr.data.getcoeff1()+")"+"X^("+(n-1)+") + ");
			else System.out.print("("+curr.data.getcoeff1()+")"+"X^("+(n-1)+")  ");

				n--;
			}
			
			

		}
	}
	public Float getcoeff()
	{
		return data.getcoeff1();
	}
	public LinkedList nextnum(LinkedList L)
	{	L=L.next;
		return L;
	}
	public LinkedList clear(LinkedList L)
	{
		L.data=null;
		L.next=null;
		return L;
	}
	
	
}
	

public class Mypolynomial {
	private static LinkedList head = new LinkedList();
	private static LinkedList head1 = new LinkedList();
	private static LinkedList head2 = new LinkedList();
	private static LinkedList head3 = new LinkedList();
	static int  d1,d2;
	
	public static void insert(LinkedList h,int n)
	{
		System.out.println("\nFor Polynomial "+n+":-\n");
		Number poly = new Number();
		poly.degree();
		for(int i=poly.getdegree();i>=0;i--)
		{	Number poly1 = new Number();
			poly1.coeff(i);
			h.append(poly1);
			poly1.setdegree(poly.getdegree());
		}
		
	}
	
	public static void add(LinkedList h1,LinkedList h2)
	{	LinkedList curr1=h1,curr2=h2;
		
		int i;
		d1=h1.count();
		d2=h2.count();
		
		if(d1>=d2)
		for(i=0;i<h1.count();curr1=curr1.nextnum(curr1),d1--,i++)
		{	
			if(d1==d2)
			{	
				Number poly2 = new Number();
				poly2.setcoeff(curr1.getcoeff()+curr2.getcoeff());
				d2--;
				head.append(poly2);
				curr2=curr2.nextnum(curr2);
			}
			else
				{	Number poly2 = new Number();
					poly2.setcoeff(curr1.getcoeff());
					head.append(poly2);
				}
			
			
		}
		else
			for(i=0;i<h2.count();curr2=curr2.nextnum(curr2),d2--,i++)
			{	
				if(d1==d2)
				{	
					Number poly2 = new Number();
					poly2.setcoeff(curr1.getcoeff()+curr2.getcoeff());
					d1--;
					head.append(poly2);
					curr1=curr1.nextnum(curr1);
				}
				else
					{	Number poly2 = new Number();
						poly2.setcoeff(curr2.getcoeff());
						head.append(poly2);
					}
				
				
			}
	}
	public static void sub(LinkedList h1,LinkedList h2)
	{	LinkedList curr1=h1,curr2=h2;
		
		int i;
		d1=h1.count();
		d2=h2.count();
		
		if(d1>=d2)
		for(i=0;i<h1.count();curr1=curr1.nextnum(curr1),d1--,i++)
		{	
			if(d1==d2)
			{	
				Number poly2 = new Number();
				poly2.setcoeff(curr1.getcoeff()-curr2.getcoeff());
				d2--;
				head.append(poly2);
				curr2=curr2.nextnum(curr2);
			}
			else
				{	Number poly2 = new Number();
					poly2.setcoeff(curr1.getcoeff());
					head.append(poly2);
				}
			
			
		}
		else
			for(i=0;i<h2.count();curr2=curr2.nextnum(curr2),d2--,i++)
			{	
				if(d1==d2)
				{	
					Number poly2 = new Number();
					poly2.setcoeff(curr1.getcoeff()-curr2.getcoeff());
					d1--;
					head.append(poly2);
					curr1=curr1.nextnum(curr1);
				}
				else
					{	Number poly2 = new Number();
						poly2.setcoeff(curr2.getcoeff());
						head.append(poly2);
					}
				
				
			}
	}
	public static void multc(LinkedList h1,int c)
	{	LinkedList curr1=h1;
		int i;
		d1=h1.count();
		
		for(i=0;i<h1.count();curr1=curr1.nextnum(curr1),d1--,i++)	
		{	
			Number poly2 = new Number();
			poly2.setcoeff(curr1.getcoeff()*c);
			head.append(poly2);
		}
	}
	public static void multp(LinkedList h1,LinkedList h2)
	{	
			LinkedList curr1=h1,curr2=h2;
		
		int i;
		d1=h1.count();
		d2=h2.count();
		int d3;
		float sum=0;
		d3=d1*d2;
		for(i=0;i<h1.count();curr1=curr1.nextnum(curr1),i++)
		{	curr2=h2;
			
			for(int j=0;j<h2.count();curr2=curr2.nextnum(curr2),j++)
			{	
				Number poly2 = new Number();
				poly2.setcoeff(curr1.getcoeff()*curr2.getcoeff());
				head.append(poly2);	
			}
		}
		LinkedList curr=head;
		for(i=0;i<d3;i++,curr=curr.nextnum(curr))
		{	if(i==0||i==(d3-1))
			{
				Number poly3 = new Number();
				poly3.setcoeff(curr.getcoeff());
				head3.append(poly3);
			}
			else 
			{
				sum=curr.getcoeff();
				curr=curr.nextnum(curr);
				sum+=curr.getcoeff();
				Number poly3 = new Number();
				poly3.setcoeff(sum);
				head3.append(poly3);
				i++;
			}
		}
		
		
		
		
		
			
	}
		
	
	
	
	
	public static void main(String args[])
	{
		int choice;	
		Scanner input = new Scanner(System.in);


		while(true)
		{
			System.out.println("\n\n1. Add two Polynomials");
			System.out.println("2. Subtract two Polynomials");
			System.out.println("3. Multiply two Polynomials");
			System.out.println("4.Multiply a Polynomial by a constant");
			System.out.println("0. Exit");
			System.out.println("Enter your choice: ");
			choice = input.nextInt();	

			switch(choice)
			{	
				case 1: head=head.clear(head);head1=head1.clear(head1);head2=head2.clear(head2);head3=head3.clear(head3);
						insert(head1,1);
						head1.display();
						insert(head2,2);
						head2.display();
						add(head1,head2);
						System.out.println("\n\nPolynomial1 + Polynomial2 is");
						head.display();
					break;
					
				case 2: head=head.clear(head);head1=head1.clear(head1);head2=head2.clear(head2);head3=head3.clear(head3);
						insert(head1,1);
						head1.display();
						insert(head2,2);
						head2.display();
						sub(head1,head2);
						System.out.println("\n\nPolynomial1 - Polynomial2 is");
						head.display();
										
					break;
				case 3: head=head.clear(head);head1=head1.clear(head1);head2=head2.clear(head2);head3=head3.clear(head3);
						insert(head1,1);
						head1.display();
						insert(head2,2);
						head2.display();
						multp(head1,head2);
						System.out.println("\n\nPolynomial1 X Polynomial2 is");
						head3.display();			
					break;
				case 4:	head=head.clear(head);head1=head1.clear(head1);head2=head2.clear(head2);head3=head3.clear(head3);
						insert(head1,1);
						head1.display();
						int c;
						System.out.println("\nEnter the Constant you Want to Multiply with: ");
						c=input.nextInt();
						multc(head1,c);
						System.out.println("\n\n"+c+"X Polynomial1 is");
						head.display();
					break;

				case 0:  //Exit
					return;

			}

		}

	}

}