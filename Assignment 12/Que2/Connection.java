
package que1.bank_gui.Connection;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;


public class Connection {
    
    java.sql.Connection con;
    Statement st;
    ResultSet rs;
    
    //Constructor for establishing connection
    public Connection(){
        
       try
       {
         String URL="jdbc:mysql://localhost:3306/bank";
         String user ="root";
         String pswd = "5029";
         
         //Registering the driver
         Class.forName("com.mysql.jdbc.Driver");
         
         //Creating a connection
         con = DriverManager.getConnection(URL, user, pswd);
         
         //Creating statement
         st = con.createStatement();  
      }
      catch(Exception e)
      {
          e.printStackTrace();
      }
    
}
    //function for adding record
    public void add(String no,String fname,String lname,String dob,String gender,String acc_type,String bal,String contact) throws Exception
    {
        try{
        String add="INSERT INTO `bank`.`records` (`Account No`, `First Name`, `Last Name`, `Gender`, `DOB`, `Contact No`, `Balance`) VALUES ('"+no+"', '"+fname+"', '"+lname+"', '"+gender+"', '"+dob+"', '"+contact+"', '"+bal+"')";
        st.executeUpdate(add);
        }
        catch(Exception ex)
        {
            //Rethrowing exception to handle in the 'myActionListener' class
            throw ex;
        }
    }
    //Function for displaying the records
    public void display()
	{
            try{
                ResultSet res = st.executeQuery("select * from records");
            while(res.next()){
		System.out.println("Account No: " +res.getString("Account No"));
		System.out.println("First Name: "+res.getString("First Name"));
                System.out.println("Last Name: "+res.getString("Last Name"));
		System.out.println("Gender: "+res.getString("Gender"));
                System.out.println("DOB: "+res.getString("DOB"));
		System.out.println("Contact No: "+res.getString("Contact No"));
                System.out.println("Balance: "+res.getString("Balance"));
                System.out.println();
                
            }   
	}
        catch(Exception e)
                    {
                        e.printStackTrace();
                    }
        }

}
