
package que1.bank_gui.GUI;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.*;

public class Bank_GUI extends JFrame {
    
    //Creating required labels and txtfields
    JLabel lblfname = new JLabel("First Name:");
    JTextField txtfnme = new JTextField(10);
    JLabel lbllname = new JLabel("Last Name:");
    JTextField txtlnme = new JTextField(10);
    JLabel lblgender = new JLabel("Gender:");
    JLabel lblbday = new JLabel("Birth Day:");
    JTextField txtbday = new JTextField(10);
    JLabel lblcontact = new JLabel("Contact No:");
    JTextField txtcont = new JTextField(10);
    JLabel lblaccno = new JLabel("Account No:");
    JTextField txtacc = new JTextField(10);
    JLabel lblamount = new JLabel ("Amount to Deposit:");
    JTextField txtamt = new JTextField(10);
    
    //Creating required radiobuttons
    JRadioButton rbtnMale = new JRadioButton("Male");
    JRadioButton rbtnFemale = new JRadioButton("Female");
    JRadioButton savings = new JRadioButton("Savings");
    JRadioButton current = new JRadioButton("Current"); 
    
    //Creating Panels for proper alignment
    JPanel radiobtn = new JPanel(new GridLayout(2,0));
    JPanel submit = new JPanel();
    JPanel txtfields = new JPanel(new GridLayout(0,2));
    JPanel menu = new JPanel(new GridLayout(2,1));
    JPanel main = new JPanel(new GridLayout(3,0));
    
    //Creating required buttons
    JButton add = new JButton("Add Record");
    JButton display = new JButton ("Display List");
    JButton sub = new JButton("Submit");
    
    //Buttongroups for gender and account selection
    ButtonGroup gender = new ButtonGroup();
    ButtonGroup acc = new ButtonGroup();
    
    //Declaring ActionListener
    myActionListener l;
    
    
      
    public Bank_GUI()
            {
                //Setting the Layout
                setLayout(new FlowLayout(FlowLayout.CENTER));
                
                //Instantiating ActionListener
                l = new myActionListener(this);
                
                //Adding buttons to buttongroups
                gender.add(rbtnMale);
                gender.add(rbtnFemale);
                acc.add(savings);
                acc.add(current);
                
                
                //adding the components to the panels
                radiobtn.add(lblgender);
                radiobtn.add(rbtnFemale);
                radiobtn.add(rbtnMale);
                radiobtn.add(new JLabel("Account:"));
                radiobtn.add(savings);
                radiobtn.add(current);
                submit.add(sub);
                txtfields.add(lblfname);
                txtfields.add(txtfnme);
                txtfields.add(lbllname);
                txtfields.add(txtlnme);
                txtfields.add(lblbday);
                txtfields.add(txtbday);
                txtfields.add(lblcontact);
                txtfields.add(txtcont);
                txtfields.add(lblaccno);
                txtfields.add(txtacc);
                txtfields.add(lblamount);
                txtfields.add(txtamt);
                
                menu.add(add);
                menu.add(display);
                
                main.add(txtfields);
                main.add(radiobtn);
                main.add(submit);
                
                
                
                //Adding panels to the frame
                add(menu);
                add(main);
                main.setVisible(false);
                
                //Adding actionlistener to the buttons
                add.addActionListener(l);
                display.addActionListener(l);
                sub.addActionListener(l);
}
    
   
    
}
     
     
     

