
package que1.bank_gui.GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.DataTruncation;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import que1.bank_gui.Connection.Connection;


public class myActionListener implements ActionListener {

    //Declaring required objects
    Connection con;
    Bank_GUI frame;
    
    myActionListener(Bank_GUI obj)
    {
        //Instantiating..
        con= new Connection();
        frame=obj;
    }
    
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        
        if(ae.getActionCommand()==frame.add.getText())
        {
            frame.menu.setVisible(false);
            frame.main.setVisible(true);
        }
        
        if(ae.getActionCommand()==frame.sub.getText())
        {
            try{
                String gen;
                String acc_type;
                if(frame.rbtnMale.isSelected()==true)
                    gen="Male";
                else
                    gen="Female";
                
                if(frame.current.isSelected()==true)
                    acc_type="Current";
                else
                    acc_type="Savings";
                
                //Calling the function for adding a record
                con.add(frame.txtacc.getText(), frame.txtfnme.getText(), frame.txtlnme.getText(), frame.txtbday.getText(),gen , acc_type, frame.txtamt.getText(), frame.txtcont.getText());
                
                //Displaying confirmation message
                JOptionPane.showMessageDialog(frame, "Record successfully added!");
                
                
                frame.main.setVisible(false);
                frame.menu.setVisible(true);
                
            }
            
            //Handling the exceptions
            catch(DataTruncation e)
            {
                JOptionPane.showMessageDialog(frame,"Please enter the date in the following format: YYYY-MM-DD");
            }
            
            catch(SQLException e)
            {
                
                JOptionPane.showMessageDialog(frame,"Record with the entered account no already exists.Please enter another account no");
                
                                }
            catch(Exception e )
            {
                e.printStackTrace();
            }
            
            
            //Setting all the textfields blank
            finally
            {
                frame.txtacc.setText("");
                frame.txtamt.setText("");
                frame.txtbday.setText("");
                frame.txtcont.setText("");
                frame.txtfnme.setText("");
                frame.txtlnme.setText("");
                        
            }
        }
        
        
        if(ae.getActionCommand()==frame.display.getText())
        {
            //Displaying the added records
            con.display();
        }
    }
    
}
