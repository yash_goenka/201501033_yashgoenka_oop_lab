package que1.bank_gui;
import que1.bank_gui.GUI.*;
import javax.swing.*;


public class Que1_Bank_SQL {
    
    public static void main(String[] args){
        
        //Creating object of the 'GUI' class
        Bank_GUI bank = new Bank_GUI();
        
        //Setting the required parameters
        bank.setSize(600,400);     
        bank.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        bank.setLocationRelativeTo(null);
        
        //Setting the visibility to 'true'
        bank.setVisible(true);
        
        //Setting the title
        bank.setTitle("Bank records");
  
  
        
  
  
  
    }
}
