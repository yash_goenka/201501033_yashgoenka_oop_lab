
package que1_employee_eventhandling.GUI;

import javax.swing.*;
import java.awt.*;


public class GUI extends JFrame {
    
    //Creating Panels
    JPanel details = new JPanel(new GridLayout(0,2));               //Panel for details
    JPanel gender = new JPanel(new GridLayout(1,0));                //Panel for gender selection
    JPanel button = new JPanel(new FlowLayout(FlowLayout.CENTER));  //Panel for buttons
    
    //Creating text fields
    JTextField txt_name = new JTextField(10);
    JTextField txt_id = new JTextField(10);
    JTextField txt_dob = new JTextField(10);
    JTextField txt_bsalary = new JTextField(10);
    JTextField txt_contact= new JTextField(10);

    JButton add = new JButton("Add Employee!");

    //Creating button group for gender selection
    ButtonGroup Gender= new ButtonGroup();
    JRadioButton male = new JRadioButton("Male");
    JRadioButton female = new JRadioButton("Female");
    JLabel lbl_exc = new JLabel();
     
    

    public GUI()
    {
        
        //Setting layout
        setLayout(new GridLayout(4,0));
        
        //Adding radiobuttons to the buttongroup
        Gender.add(male);
        Gender.add(female);
        
        //Adding labels and textfiels to panel 'details'
        details.add(new JLabel("Employee Number"));
        details.add(txt_id);
        details.add(new JLabel("Name"));
        details.add(txt_name);
        details.add(new JLabel("Date of Birth"));
        details.add(txt_dob);
        details.add(new JLabel("Contact Number"));
        details.add(txt_contact);
        details.add(new JLabel("Basic Salary"));
        details.add(txt_bsalary);
       
        
        //Adding RadioButtons to panel 'gender'
        gender.add(new JLabel("Gender"));
        gender.add(male);
        gender.add(female);
        
        
        
        
        
        //Adding button to panel 'button'  
          button.add(add);
          
        
        //Adding all the panels to the frame
        add(details);
        add(gender);
        add(button);
        add(lbl_exc);
        
        
        //Setting the title 
        setTitle("Employee");
        
        //Setting the size of the frame
        setSize(600,400);
        
        //Terminates the progran on closing the window
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //Setting the location of the frame to 'Centre'
        setLocationRelativeTo(null);
    }
    
}
