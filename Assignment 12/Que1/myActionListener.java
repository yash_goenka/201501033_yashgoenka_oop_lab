
package que1_employee_eventhandling.GUI;
import com.mysql.jdbc.MysqlDataTruncation;
import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import que1_employee_eventhandling.Employee_Logic.*;


public class myActionListener implements ActionListener {

    //Initializing the required objects
    Main_Menu m;
    GUI form=null;
    DatabaseConnection d;
    GUI update=null;
     myActionListener(Main_Menu m,GUI form)
    {
        this.m=m;
        this.form=form;
        d=new DatabaseConnection();
    }

    
    
    @Override
    public void actionPerformed(ActionEvent ae) 
    {
        if (ae.getActionCommand() == null ? m.btn_add.getText() == null : ae.getActionCommand().equals(m.btn_add.getText()))
        {
            form.add.setText("Add Employee!");
            //Setting the visibility of the frame to 'true'
            form.setVisible(true);
        }
        
        if(ae.getActionCommand() == "Add Employee!")
        {
            
            Employee emp;
            emp= new Employee();
            try{
                form.lbl_exc.setVisible(false);
                emp.setEmployee_no(form.txt_id.getText());
                emp.setDob(form.txt_dob.getText());
                emp.setContact_no(form.txt_contact.getText());
                emp.setName(form.txt_name.getText());
                emp.setSalary(Float.parseFloat(form.txt_bsalary.getText()));
            if(form.male.isSelected()==true)
                emp.setEmployee_gender("Male");
            else
                emp.setEmployee_gender("Female");
            
            //Calling 'add' function from 'DatabaseConnection' class for adding the employee in the database
            d.add(emp);
            
            //Showing confirmation message
            JOptionPane.showMessageDialog(form, "Employee Added!");
            form.setVisible(false);
            }
            
            //Caching all the exceptions
            catch(NumberFormatException e)
            {
                System.out.println(e);
                form.lbl_exc.setText("Please fill the information correctly");
                form.lbl_exc.setVisible(true);
            }
            
            catch(MySQLIntegrityConstraintViolationException e)
                        {
                           form.lbl_exc.setText("Employee with the entered ID already exists. Please enter new ID ");
                           form.lbl_exc.setVisible(true);
                        }
            
            
            catch(MysqlDataTruncation e)
            {
                form.lbl_exc.setText("Please enter the date in the following format: YYYY-MM-DD");
                form.lbl_exc.setVisible(true);
            }
                
            catch (Exception ex) {
                System.out.println(ex);
                form.lbl_exc.setText("Employee with the entered ID already. Please enter new ID ");
            }
            //emp.display_employee();
            form.txt_bsalary.setText("");
            form.txt_contact.setText("");
            form.txt_dob.setText("");
            
            form.txt_id.setText("");
            
            form.txt_name.setText("");
            
        }
        if(ae.getActionCommand() == null ? m.btn_display.getText() == null : ae.getActionCommand().equals(m.btn_display.getText()))
        {
            //Calling function for displaying employee list
            d.display_employee();
        }
        
        if(ae.getActionCommand() == null ? m.btn_update.getText() == null : ae.getActionCommand().equals(m.btn_update.getText()))
        {
            m.btn_submit.setText("Submit");
            m.button.setVisible(false);
            m.update.setVisible(true);
        }
        
        if(ae.getActionCommand() == "Submit")
        {
            
            
            boolean t=false;
            try{
                m.lbl_exc.setVisible(false);
                
                //Caaling the 'search' function for searching the employee to be updated
                t=d.search(m.txt_id.getText());
            }
            
            //Cayching the exceptions
            catch(NumberFormatException e)
            {
                System.out.println(e);
                m.lbl_exc.setText("Please fill the information correctly");
                m.lbl_exc.setVisible(true);
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            
            if(t==true)
            {
                
                form.add.setText("Update!");
                form.setVisible(true);
            }
            
            else if(t==false)
            {
                m.lbl_exc.setText("Employee not found!");
                m.lbl_exc.setVisible(true);
            }
                
        }
        
        if(ae.getActionCommand()=="Update!")
        {
                Employee emp=new Employee();
                
                try{
                form.lbl_exc.setVisible(false);
                emp.setEmployee_no(form.txt_id.getText());
                emp.setDob(form.txt_dob.getText());
                emp.setContact_no(form.txt_contact.getText());
                emp.setName(form.txt_name.getText());
                System.out.println("Salary="+form.txt_bsalary.getText());
                emp.setSalary(Float.parseFloat(form.txt_bsalary.getText()));
                m.button.setVisible(true);
                m.update.setVisible(false);
                
                
                if(form.male.isSelected()==true)
                    emp.setEmployee_gender("Male");
                else
                    emp.setEmployee_gender("Female");
            
                d.update(m.txt_id.getText(),emp);
                JOptionPane.showMessageDialog(form, "Information updated ");
                form.setVisible(false);
            }
            
            catch(NumberFormatException e)
            {
                e.printStackTrace();
                form.lbl_exc.setText("Please fill the information correctly");
                form.lbl_exc.setVisible(true);
            }
            
            catch(MySQLIntegrityConstraintViolationException e)
                        {
                           form.lbl_exc.setText("Employee with the entered ID already exists. Please enter new ID ");
                           form.lbl_exc.setVisible(true);
                        }
            
            
            catch(MysqlDataTruncation e)
            {
                form.lbl_exc.setText("Please enter the date in the following format: YYYY-MM-DD");
                form.lbl_exc.setVisible(true);
            }
                
            catch (Exception ex) {
                System.out.println(ex);
                form.lbl_exc.setText("Employee with the entered ID already. Please enter new ID ");
            }
            //emp.display_employee();
            form.txt_bsalary.setText("");
            form.txt_contact.setText("");
            form.txt_dob.setText("");
            
            form.txt_id.setText("");
            
            form.txt_name.setText("");
            
        }
        
        if(ae.getActionCommand() == m.btn_delete.getText())
        {
            
            m.button.setVisible(false);
            m.update.setVisible(true);
            m.btn_submit.setText("Delete!");
            
        }
        
        if(ae.getActionCommand()=="Delete!"){
            boolean t;
            try{
                //Searching for the employee to be deleted
                t=d.search(m.txt_id.getText());
                if(t==true)
                {
                    //if employee is found 'delete' function is called
                    d.delete(m.txt_id.getText());
                    
                    //Displaying confiramtion message
                    JOptionPane.showMessageDialog(m, "Employee record deleted successfully");
                    
                    
                    m.update.setVisible(false);
                    m.button.setVisible(true);
                }
                else if(t==false)
                {
                    //Alerting the user that the entered employee was not found
                    m.lbl_exc.setText("Employee not found!");
                    m.lbl_exc.setVisible(true);
                }
            }
            
            
            catch(Exception e)
            {
                JOptionPane.showMessageDialog(m, e.getLocalizedMessage());
            }
            
            finally
            {
                m.txt_id.setText("");
            }
            
            
        }
}
}