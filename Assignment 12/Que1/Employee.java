
package que1_employee_eventhandling.Employee_Logic;

    public class Employee
{
    private String employee_no;
    private String name;
    private String employee_gender;
    private String dob;
    private String contact_no;
    private float salary;
    float DA=(60*salary)/100;
    float HRA=(12*salary)/100;
    float TA = 12000;
    float PT = 200;
    private float gross_salary;
    float PF;
    
    float TDS;
    float total_deduction;
    private float net_salary;
    
  


	public void display_employee()
	{
		System.out.println("Employee No: " +getEmployee_no());
		System.out.println("Employee's Name: "+getName());
		System.out.println("Employee's Gender: "+getEmployee_gender());
                System.out.println("Employee's DOB: "+getDob());
		System.out.println("Employee's Contact No: "+getContact_no());
                System.out.println("Employee's Salary: "+getSalary());
                System.out.println("Gross Salary: "+getGross_salary());
                System.out.println("Net Salary: "+getNet_salary());

	}

    /**
     * @return the employee_no
     */
    public String getEmployee_no() {
        return employee_no;
    }

    /**
     * @param employee_no the employee_no to set
     */
    public void setEmployee_no(String employee_no) {
        this.employee_no = employee_no;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the employee_gender
     */
    public String getEmployee_gender() {
        return employee_gender;
    }

    /**
     * @param employee_gender the employee_gender to set
     */
    public void setEmployee_gender(String employee_gender) {
        this.employee_gender = employee_gender;
    }

    /**
     * @return the dob
     */
    public String getDob() {
        return dob;
    }

    /**
     * @param dob the dob to set
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     * @return the contact_no
     */
    public String getContact_no() {
        return contact_no;
    }

    /**
     * @param contact_no the contact_no to set
     */
    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    /**
     * @return the salary
     */
    public float getSalary() {
        return salary;
    }

    /**
     * @param salary the salary to set
     */
    public void setSalary(float salary) {
        this.salary = salary;
        gross_salary=salary + DA + HRA + TA;
        PF= (10*salary)/100;
        TDS= (10*(gross_salary - TA - PF - PT))/100;
        total_deduction= PF + PT + TDS;
        net_salary= gross_salary - total_deduction;
    }

    /**
     * @return the gross_salary
     */
    public float getGross_salary() {
        return gross_salary;
    }

    /**
     * @param gross_salary the gross_salary to set
     */
    public void setGross_salary(float gross_salary) {
        this.gross_salary = gross_salary;
    }

    /**
     * @return the net_salary
     */
    public float getNet_salary() {
        return net_salary;
    }

    /**
     * @param net_salary the net_salary to set
     */
    public void setNet_salary(float net_salary) {
        this.net_salary = net_salary;
    }
}

	