
package que1_employee_eventhandling.Employee_Logic;
import java.sql.*;

public class DatabaseConnection   {
    
    Connection con;
    Statement st;
    ResultSet rs;
    public DatabaseConnection(){
        
   try
      {
         String URL="jdbc:mysql://localhost:3306/employee";
         String user ="root";
         String pswd = "5029";
         
         //Registering the driver
         Class.forName("com.mysql.jdbc.Driver");
         
         //Creating a connection
         con = DriverManager.getConnection(URL, user, pswd);
         
         //Creating statement
         st = con.createStatement();  
      }
      catch(Exception e)
      {
          e.printStackTrace();
      }
}
   
    
    //Creating function for adding employee into the database
    public void add(Employee e) throws Exception
    {
        try{
        String add="INSERT INTO `employee`.`details` (`ID`, `Name`, `Gender`, `Contact No`, `DOB`, `Basic Salary`, `Gross Salary`, `Net Salary`) VALUES ('"+e.getEmployee_no()+"', '"+e.getName()+"', '"+e.getEmployee_gender()+"', '"+e.getContact_no()+"', '"+e.getDob()+"', '"+e.getSalary()+"', '"+e.getGross_salary()+"', '"+e.getNet_salary()+"')";
        st.executeUpdate(add);
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }
    
    
    //Creating function for displaying the list of employees 
    public void display_employee()
	{
            try{
                ResultSet res = st.executeQuery("select * from details");
            while(res.next()){
		System.out.println("Employee No: " +res.getString("ID"));
		System.out.println("Employee's Name: "+res.getString("Name"));
		System.out.println("Employee's Gender: "+res.getString("Gender"));
                System.out.println("Employee's DOB: "+res.getString("DOB"));
		System.out.println("Employee's Contact No: "+res.getString("Contact No"));
                System.out.println("Employee's Salary: "+res.getString("Basic Salary"));
                System.out.println("Gross Salary: "+res.getString("Gross Salary"));
                System.out.println("Net Salary: "+res.getString("Net Salary"));
            }   
	}
        catch(Exception e)
                    {
                        e.printStackTrace();
                    }
        }
    
    //Function for searching employee
    public boolean search(String id) throws Exception
    {
        boolean t=false;
        try{
            ResultSet res = st.executeQuery("select * from details");
            while(res.next())
            {
               if(String.valueOf(res.getString("ID")) == null ? id == null : String.valueOf(res.getString("ID")).equals(id)) 
               {
                   t=true;   
               }  
            }
        }
        
        catch(Exception e)
        {
            
            e.printStackTrace();
            return t;
            
        }
        return t;
        
    }
    
    //Fuunction for updating info of a existing employee
    public void update(String id,Employee e) throws Exception
    {
        try{
        String add="UPDATE `employee`.`details` SET `ID`='"+e.getEmployee_no()+"', `Name`='"+e.getName()+"', `Gender`='"+e.getEmployee_gender()+"', `Contact No`='"+e.getContact_no()+"', `Basic Salary`='"+e.getSalary()+"', `Gross Salary`='"+e.getGross_salary()+"', `Net Salary`='"+e.getNet_salary()+"', `DOB`='"+e.getDob()+"' WHERE `ID`='"+id+"'";
        st.executeUpdate(add);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
    }
    
    //Function for deleting a employee from the database 
    public void delete(String id) throws Exception
    {
        try{
        String add="DELETE FROM `employee`.`details` WHERE `ID`='"+id+"'";
        st.executeUpdate(add);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
    }
}