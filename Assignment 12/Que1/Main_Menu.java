
package que1_employee_eventhandling.GUI;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class Main_Menu extends JFrame{
    
    JButton btn_add = new JButton("Add Employee");
    JButton btn_display = new JButton("Display Details");
    JButton btn_update = new JButton("Update");
    JButton btn_delete = new JButton("Delete");
    JPanel button = new JPanel(new GridLayout(4,0));
    JPanel update = new JPanel(new GridLayout(3,0));
    JLabel lbl_id = new JLabel("Enter ID");
    JTextField txt_id = new JTextField(15);
    JButton btn_submit = new JButton("Submit");
    JLabel lbl_exc = new JLabel();
   public myActionListener menu;
    
    public Main_Menu()
    {
        //Creating instance of 'GUI' class
        GUI form = new GUI();
        
        //Creating actionListener
        menu = new myActionListener(this,form);
        
        //Setting Layout of the frame
        setLayout(new FlowLayout(FlowLayout.CENTER));
        
        //Adding button to the 'button' panel
        button.add(btn_add);
        button.add(btn_display);
        button.add(btn_update);
        button.add(btn_delete);
       
        //Adding components to the 'update' panel
        update.add(lbl_id);
        update.add(txt_id);
        update.add(btn_submit);
        update.add(lbl_exc);
        
        //Adding panels to the frame
        add(button);
        add(update);
        update.setVisible(false);
        
        //Adding actionListener
        btn_add.addActionListener(menu);
        btn_display.addActionListener(menu);
        form.add.addActionListener(menu);
        btn_submit.addActionListener(menu);
        btn_update.addActionListener(menu);
        btn_delete.addActionListener(menu);
    }
    
}
