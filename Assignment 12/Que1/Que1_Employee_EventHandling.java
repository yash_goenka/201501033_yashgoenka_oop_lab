
package que1_employee_eventhandling;

import javax.swing.JFrame;
import que1_employee_eventhandling.GUI.*;


public class Que1_Employee_EventHandling {

   
    public static void main(String[] args) {
        
        
        
    
        //Creating instance of 'Main_Menu'
        Main_Menu frame1 = new Main_Menu();
        
        //Setting size
        frame1.setSize(600,400);
        
        //Terminating the program on closing frame
        frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //Setting locationg of frame
        frame1.setLocationRelativeTo(null);
        
        //Setting visibility to 'true'
        frame1.setVisible(true);
    }
    
}
