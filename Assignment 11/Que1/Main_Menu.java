package que1_employee_eventhandling.GUI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import que1_employee_eventhandling.GUI.GUI;
import que1_employee_eventhandling.GUI.myActionListener;


public class Main_Menu extends JFrame{
    
    JButton btn_add = new JButton("Add Employee");
    JButton btn_display = new JButton("Display Details");
    JPanel button = new JPanel(new GridLayout(2,0));
    
    
    
   public myActionListener menu;
    
    public Main_Menu()
    {
        GUI form = new GUI();
        menu = new myActionListener(this,form);
       setLayout(new FlowLayout(FlowLayout.CENTER));
        
       

        button.add(btn_add);
        button.add(btn_display);
        
       
        
        add(button);
        btn_add.addActionListener(menu);
       btn_display.addActionListener(menu);
        form.add.addActionListener(menu);
    }
    
}
