package que1_employee_eventhandling.Employee_Logic;


public class LinkedList
{
	private Employee data = null;
	private LinkedList next = null;

	public void append(Employee newData)
	{
		if(data == null)
			this.data = newData;
		else
		{
			LinkedList curr;
			for(curr = this; curr.next!=null; curr = curr.next);

			curr.next = new LinkedList();
			curr = curr.next;
			curr.data = newData;
			curr.next = null;
		}
	}

	public void display()
	{
            
		if(this.data == null)
			System.out.println("\nList is Empty!");
		else
		{
			LinkedList curr;
			for(curr = this; curr!=null; curr = curr.next)
			{
				curr.data.display_employee();
				
			}
		}
        }}
