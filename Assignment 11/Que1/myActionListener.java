package que1_employee_eventhandling.GUI;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import que1_employee_eventhandling.Employee_Logic.*;


public class myActionListener implements ActionListener {

    
    Main_Menu m;
    GUI form=null;
    LinkedList l_emp = new LinkedList();;
    
     myActionListener(Main_Menu m,GUI form)
    {
        this.m=m;
        this.form=form;
       
    }

    
    
    @Override
    public void actionPerformed(ActionEvent ae) 
    {
     
        
        if (ae.getActionCommand() == null ? m.btn_add.getText() == null : ae.getActionCommand().equals(m.btn_add.getText()))
        {
            
            
            
            //Setting the title 
        form.setTitle("Employee");
        
        //Setting the size of the frame
        form.pack();
        
        //Terminates the progran on closing the window
        form.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //Setting the location of the frame to 'Centre'
        form.setLocationRelativeTo(null);
        
        //Setting the visibility of the frame to 'true'
        form.setVisible(true);
        
        
        }
        
        
        
        if(ae.getActionCommand() == null ? form.add.getText() == null : ae.getActionCommand().equals(form.add.getText()))
        {
            Employee emp;
            emp= new Employee();
            
            emp.employee_no=Integer.parseInt(form.txt_id.getText());
            emp.dob=form.txt_dob.getText();
            emp.contact_no=form.txt_contact.getText();
            emp.name=form.txt_name.getText();
            emp.salary=Float.parseFloat(form.txt_bsalary.getText());
            if(form.male.isSelected()==true)
                emp.employee_gender="Male";
            else
                emp.employee_gender="Female";
            
     
            
            l_emp.append(emp);
            
            form.setVisible(false);
            //emp.display_employee();
            form.txt_bsalary.setText("");
            form.txt_contact.setText("");
            form.txt_dob.setText("");
            
            form.txt_id.setText("");
            
            form.txt_name.setText("");
            
        }
        if(ae.getActionCommand()==m.btn_display.getText())
        {
            
            l_emp.display();
        }
        
        
       
        
    }
}