package que1_employee_eventhandling.Employee_Logic;

import java.util.Scanner;

    public class Employee
{
    public int employee_no;
    public String name;
    public String employee_gender;
    public String dob;
    public String contact_no;
    public float salary;
    float DA=(60*salary)/100;
    float HRA=(12*salary)/100;
    float TA = 12000;
    public float gross_salary=salary + DA + HRA + TA;
    float PF= (10*salary)/100;
    float PT = 200;
    float TDS= (10*(gross_salary - TA - PF - PT))/100;
    float total_deduction= PF + PT + TDS;
    public float net_salary= gross_salary - total_deduction;
    
  


	public void display_employee()
	{
		System.out.println("Employee No: " +employee_no);
		System.out.println("Employee's Name: "+name);
		System.out.println("Employee's Gender: "+employee_gender);
                System.out.println("Employee's DOB: "+dob);
		System.out.println("Employee's Contact No: "+contact_no);
                System.out.println("Employee's Salary: "+salary);
                System.out.println("Gross Salary: "+gross_salary);
                System.out.println("Net Salary: "+net_salary);

	}
}
