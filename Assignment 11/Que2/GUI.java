package que2.stack_gui_evnthndln.GUI;

import javax.swing.*;
import java.awt.*;
import que2.stack_gui_evnthndln.Logic.Stack;

public class GUI extends JFrame {
    
    //Creating GUI components
    JPanel p = new JPanel(new GridLayout(3,0));  
    JPanel button = new JPanel(new FlowLayout(FlowLayout.CENTER)); //Panel for Buttons
    JButton push = new JButton("Push");
    JButton pop = new JButton("Pop");
    JButton peep = new JButton("Peep");
    JLabel lbl_ex= new JLabel("Stack is full"); 
    JLabel st = new JLabel();
    JTextField value = new JTextField(10);
    JLabel top = new JLabel();
    public GUI(int length)
    {
        //Creating an object of 'Stack' class
        Stack stack=new Stack(length) ;
        
        //Creating actionlistener
        myActionListener l = new myActionListener(this, stack);
        

       //Setting layout
        setLayout(new GridLayout(3,0));
        
        //Adding components to the panel 'p'
        p.add(new JLabel("Element"));
        p.add(value);
        p.add(st);
        p.add(top);
        p.add(lbl_ex);
        

        //Adding components to the panel 'button'
        button.add(pop);
        button.add(push);
        button.add(peep);
        
        //Adding actionlistener for buttons
        push.addActionListener(l);
        pop.addActionListener(l);
        peep.addActionListener(l);
        
        //Adding panels to the frame
        add(p);
        add(button);
        
        lbl_ex.setVisible(false);
        
        
        
        
        
    }

}