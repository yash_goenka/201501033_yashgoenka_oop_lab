package que2.stack_gui_evnthndln;

import java.util.Scanner;
import javax.swing.JFrame;
import que2.stack_gui_evnthndln.GUI.GUI;


public class Que2Stack_GUI_EvntHndln {

    
    public static void main(String[] args) {
        
        //Accepting the size of the stack from the user
        System.out.println("Enter the size of the stack");
        Scanner in = new Scanner (System.in);
       
        int size=0,cnt=0;
        
        //Handling 'InputMismatchException'
        do{
            try
                {
                size = in.nextInt();
                cnt++;
                }
        
            catch (Exception e)
                {
                    System.out.println("Please enter an integer");
                    in.nextLine();
                }
        }while(cnt==0);
        
        
        //Creating object of the class 'GUI'
        GUI Stack = new GUI(size);
        
        //Setting the size of the frame
        Stack.setSize(600,400);
        
        
        //Terminates the progran on closing the window
        Stack.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //Setting the location of the frame to 'Centre'
        Stack.setLocationRelativeTo(null);
        
        //Setting title
        Stack.setTitle("Stack");
        
        
        //Setting the visibility of the frame to 'true'
        Stack.setVisible(true);
    }
    
}
