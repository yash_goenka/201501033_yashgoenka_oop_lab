package que2.stack_gui_evnthndln.Logic;


import java.lang.Exception;

public class Stack
{
    int [] stack;
    private int size;
    int top;
    
    
    public Stack(int i)                
    {
        size = i;
        stack = new int[size];
        top = -1;
    }
    
    
    public void push(int val) throws Exception              
    {
        
      
        try
        {
            if(top!=stack.length-1)
            stack[++top] = val;
            else
                throw new Exception();
        }
        catch(Exception e)
        {
            throw e;
        }
        
        
        
            
            
    }   
    
    public String display()                
    {
        int i;
        String str;
        
        str="";
        for(i=0;i<=top;i++)
        {
            if(i==top)
                str +=stack[i];
            
            else
                str+=""+stack[i]+",";
                
        }
        return str;
    }
    
    
    public void pop() throws Exception                
    {
        try{
        if(top!=-1)
        top--;
        else
             throw new Exception();
        }
        catch(Exception e)
        {
            throw e;
        }
    }
    
    
    public int peep()                   
    {
        return stack[top];
    }
    
    
    public void emptycheck()                         
    {
        if(top==-1)
            System.out.println("\nThe stack is empty!\n");
        
        else
            System.out.println("\nThe stack is not empty!\n");
    }
    
    
    public void fullcheck()                         
    {
        if(top==stack.length-1)
            System.out.println("\nThe stack is full!\n");
        
        else
            System.out.println("\nThe stack is not full!\n");
    }

    /**
     * @return the size
     */
    public int getSize() {
        return size;
    }
    
    
}