package que2.stack_gui_evnthndln.GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import que2.stack_gui_evnthndln.Logic.*;


public class myActionListener implements ActionListener {

    GUI gstack ;
    Stack stack;
    
    
    public myActionListener(GUI s, Stack stack)
    {
        
        this.stack=stack;
        gstack =s;
        
    }
    @Override
    public void actionPerformed(ActionEvent ae)
    {
        if(ae.getActionCommand()=="Push")
        { 
            try{
            stack.push(Integer.parseInt(gstack.value.getText()));
            gstack.lbl_ex.setVisible(false);
            gstack.top.setVisible(false);
            gstack.st.setText("The entered Stack is:  "+stack.display());   
            }
            catch(NumberFormatException e)
            {
                gstack.lbl_ex.setText("Please enter an integer");
                gstack.lbl_ex.setVisible(true);
            }
            catch(Exception e){
                gstack.lbl_ex.setText("Stack is full");
                gstack.lbl_ex.setVisible(true);
            }
            finally
            {
                gstack.value.setText("");
            }
            
        }
        
        else if(ae.getActionCommand()=="Pop")
        
        {
                
            try {
                gstack.lbl_ex.setVisible(false);
                stack.pop();
                
            } catch (Exception ex) {
                gstack.lbl_ex.setText("Stack is Empty");
                gstack.lbl_ex.setVisible(true);
            }
            gstack.st.setText("The entered Stack is:  "+stack.display());
        }   
        
        else if(ae.getActionCommand()=="Peep")
        {
            try
            {
            gstack.lbl_ex.setVisible(false);    
            gstack.top.setText("The topmost element is " + stack.peep());
            gstack.top.setVisible(true);
            }
            catch(Exception e)
            {
                gstack.top.setVisible(false);
                gstack.lbl_ex.setText("The stack is empty");
                gstack.lbl_ex.setVisible(true);
            }
            }
    }
    
}