package circle;

import javax.swing.*; 
import java.awt.*;
import java.awt.Color;
import java.awt.event.*; 

import queue.queue_input;

public class circular_queue extends JFrame
{
	JButton enq = new JButton("EnQueue"); 
	JButton deq= new JButton("DeQueue");
	 
	JLabel lblNum = new JLabel("Insert a Number to list in queue");
	JTextField txtNum = new JTextField(20);  
	display circular_queue = new display(); 
	
	String input = JOptionPane.showInputDialog("SIZE?"); 
	public  int  size =Integer.parseInt(input);

	queue_input object = new queue_input(size);
	public circular_queue() 
	{ 
		JPanel display = new JPanel(); 
	      
		deq.setBackground(Color.RED);
		display.add(lblNum);
		display.add(txtNum);
		display.add(enq);
		enq.setBackground(Color.RED);
		display.add(deq);
		
		this.add(circular_queue, BorderLayout.CENTER); 
		this.add(display, BorderLayout.SOUTH); 
		enq.addActionListener(new a_l());
		deq.addActionListener(new a_l());
	}
	class display extends JPanel
	{
		queue_input object;
		Label l1;
		@Override 
		protected void paintComponent(Graphics g) 
		{
			super.paintComponent(g);    

			g.setColor(Color.ORANGE);
			g.fillOval(100,100,300,300);
			g.setColor(Color.RED);
			g.setFont(new Font("san sarif", Font.BOLD, 25));
			int i;
			if(object!=null)
			{
				for(i=object.front;i<object.a.length;i++)
		        {   
					if(object.a[i] != 0)
					{
						 if(object.front == i)
							 g.setColor(Color.RED);
		                 else
		                	 g.setColor(Color.CYAN); 
						 g.drawString(Integer.toString(object.a[i]), (int)(250+120*Math.cos(2*i*Math.PI/object.s)) , (int)(270-100*Math.sin(2*i*Math.PI/object.s)));
		            } 
		        }
				if(object.isfull())
				{
					g.setColor(Color.YELLOW);
					String str="";
					str+="SORRY QUEUE FILLED COMPLETELY";
					g.drawString(str,120 + 650, 300);
				}
				if(object.isempty())
				{
					g.setColor(Color.YELLOW);
					String str="";
					str+="QUEUE IS EMPTIED";
					g.drawString(str,120 + 400, 300);
				}
			}
		}
		public void dequeue(queue_input object)
		{
			this.object = object;
			repaint(); 
		}
		public void enqueue(queue_input object)
		{
			this.object = object;
			repaint();
		}
	}

	class a_l implements ActionListener 
	{	
		public void actionPerformed(ActionEvent event) 
	    {
			String actionCommand = event.getActionCommand(); 
			if(event.getActionCommand() == enq.getText())
			{  
				int x = Integer.parseInt(txtNum.getText());
				object.enqeue(x);
				circular_queue.enqueue(object);
			}
			else if(event.getActionCommand()==deq.getText()) 
			{
				int y= object.dequeue();
	            circular_queue.dequeue(object);
			}
	    }
	}
}
	 
	   
	 

