package lab11_3;

import javax.swing.JFrame;

import circle.circular_queue;
import queue.queue_input;

public class lab11_3 
{
	public static void main(String[] args)
	{ 
		JFrame frame = new circular_queue(); 
	    frame.setTitle("Circular Queue");
	     
	    frame.setLocationRelativeTo(null); 
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
	    frame.setSize(1000, 1000); 
	    frame.setVisible(true);  
	} 
}
