
package que1.bank_gui.GUI;

import java.awt.GridLayout;
import javax.swing.*;

public class Bank_GUI extends JFrame {
    
    //Creating required labels and txtfields
    JLabel lblfname = new JLabel("First Name:");
    JTextField txtfnme = new JTextField(10);
    JLabel lbllname = new JLabel("Last Name:");
    JTextField txtlnme = new JTextField(10);
    JLabel lblgender = new JLabel("Gender:");
    JLabel lblbday = new JLabel("Birth Day:");
    JTextField txtbday = new JTextField(10);
    JLabel lblcontact = new JLabel("Contact No:");
    JTextField txtcont = new JTextField(10);
    JLabel lblaccno = new JLabel("Account No:");
    JTextField txtacc = new JTextField(10);
    JLabel lblamount = new JLabel ("Amount to Deposit:");
    JTextField txtamt = new JTextField(10);
    
    //Creating required radiobuttons
    JRadioButton rbtnMale = new JRadioButton("Male");
    JRadioButton rbtnFemale = new JRadioButton("Female");
    
    //Creating Panels for proper alignment
    JPanel radiobtn = new JPanel(new GridLayout(2,0));
    JPanel submit = new JPanel();
    JPanel txtfields = new JPanel(new GridLayout(0,2));
        
    
      
    public Bank_GUI()
            {
                //Setting the Layout
                setLayout(new GridLayout(3,0,0,0));
                
                //adding the components to the panels
                radiobtn.add(lblgender);
                radiobtn.add(rbtnFemale);
                radiobtn.add(rbtnMale);
                radiobtn.add(new JLabel("Account:"));
                radiobtn.add(new JRadioButton("Savings"));
                radiobtn.add(new JRadioButton("Current"));
                submit.add(new JButton("Submit"));
                txtfields.add(lblfname);
                txtfields.add(txtfnme);
                txtfields.add(lbllname);
                txtfields.add(txtlnme);
                
                txtfields.add(lblbday);
                txtfields.add(txtbday);
                txtfields.add(lblcontact);
                txtfields.add(txtcont);
                txtfields.add(lblaccno);
                txtfields.add(txtacc);
                txtfields.add(lblamount);
                txtfields.add(txtamt);
                
                //Adding panels to the frame
                add(txtfields);
                add(radiobtn);
                add(submit);
                
        
                
                
}
    
   
    
}
     
     
     

