
package que1.bank_gui;
import que1.bank_gui.GUI.*;
import javax.swing.*;


public class Que1_BankGUI {
    
    public static void main(String[] args){
        
        //Creating object of the 'GUI' class
        Bank_GUI bank = new Bank_GUI();
        
        //Setting the required parameters
        bank.pack();     
        bank.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        bank.setLocationRelativeTo(null);
        
        //Setting the visibility to 'true'
        bank.setVisible(true);
    }
}
