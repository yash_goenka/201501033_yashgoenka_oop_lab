
package que2.store_gui.GUI;

import javax.swing.*;
import java.awt.*;

public class StoreGUI extends JFrame {
    
    //Creating panels
    JPanel p1 = new JPanel(new GridLayout(0,2)); //Panel for details
    JPanel btn = new JPanel(new FlowLayout(FlowLayout.CENTER)); //Panel for 'Finish' button
    JPanel payment = new JPanel(new GridLayout(1,0));// Panel for 'mode of payment' option
    JButton finish = new JButton("Finish");
    
    public StoreGUI()
    {
        //Setting Layout
        setLayout(new GridLayout(3,0));
        
        //Adding Labels and TextFields to panels
        p1.add(new JLabel("Customer Name"));
        p1.add(new JTextField(10));
        p1.add(new JLabel("Customer ID"));
        p1.add(new JTextField(10));
        p1.add(new JLabel("Contact No") );
        p1.add(new JTextField(10));
        p1.add(new JLabel("Product ID"));
        p1.add(new JTextField(10));
        p1.add(new JLabel("Product Name"));
        p1.add(new JTextField(10));
        p1.add(new JLabel("Quantity"));
        p1.add(new JTextField(10));
        p1.add(new JLabel("Product Price"));
        p1.add(new JTextField(10));
        p1.add(new JLabel("Total Bill"));
        p1.add(new JTextField(10));
        finish.setFont(new Font("SansSerif",Font.BOLD,20));  //Setting font for the 'Finish' button
        btn.add(finish);
       payment.add(new JLabel("Mode of Payment     "));
       payment.add(new JRadioButton("Credit Card"));
       payment.add(new JRadioButton("Debit Card"));
       payment.add(new JRadioButton("Cash"));
       payment.add(new JRadioButton("Other"));
       payment.setSize(400, 200);
       
       //Adding panels to the frame
        add(p1);
        add(payment);
        add(btn);
        
        
    }
    
    
}
