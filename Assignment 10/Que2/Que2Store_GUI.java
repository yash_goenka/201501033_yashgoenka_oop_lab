
package que2.store_gui;

import javax.swing.JFrame;
import que2.store_gui.GUI.StoreGUI;


public class Que2Store_GUI {

    
    public static void main(String[] args) {
        
        //Creating Object of 'StoreGUI' class
        StoreGUI store = new StoreGUI();
        
        
        store.setSize(600,400);               //Setting size of the frame
        store.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  //Terminates the program on closing the frame
        store.setLocationRelativeTo(null);    //Setting location of the frame to 'Centre'
        store.setVisible(true);               //Setting the visibility to 'true'
    }
    
}
