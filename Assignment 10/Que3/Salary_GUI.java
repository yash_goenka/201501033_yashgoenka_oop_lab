
package que3.salary.GUI;

import javax.swing.*;
import java.awt.*;


public class Salary_GUI extends JFrame {
    
    //Creating Panels
    JPanel details = new JPanel(new GridLayout(0,2));      //Panel for details
    JPanel gender = new JPanel(new GridLayout(1,0));       //Panel for gender selection
    JPanel button = new JPanel(new FlowLayout(FlowLayout.CENTER));  //Panel for buttons
    public Salary_GUI()
    {
        
        //Setting layout
        setLayout(new GridLayout(3,0));
        
        //Adding labels and textfiels to panel 'details'
        details.add(new JLabel("Employee Number"));
        details.add(new JTextField(10));
        details.add(new JLabel("Name"));
        details.add(new JTextField(10));
        details.add(new JLabel("Date of Birth"));
        details.add(new JTextField(10));
        details.add(new JLabel("Contact Number"));
        details.add(new JTextField(10));
        details.add(new JLabel("Basic Salary"));
        details.add(new JTextField(10));
        
        //Adding RadioButtons to panel 'gender'
        gender.add(new JLabel("Gender"));
        gender.add(new JRadioButton("Male"));
        gender.add(new JRadioButton("Female"));
        
        //Adding button to panel 'button'  
        button.add(new JButton("Calculate Net Salary"));
        button.add(new JButton("Calculate Gross Salary"));
        
        //Adding all the panels to the frame
        add(details);
        add(gender);
        add(button);
    }
    
}
