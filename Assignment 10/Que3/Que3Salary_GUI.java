
package que3.salary;

import javax.swing.JFrame;
import que3.salary.GUI.Salary_GUI;


public class Que3Salary_GUI {

    
    public static void main(String[] args) {
        
        //Creating object of the class 'Salary_GUI'
        Salary_GUI form = new Salary_GUI();
        
        //Setting the size of the frame
        form.pack();
        
        //Terminates the progran on closing the window
        form.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //Setting the location of the frame to 'Centre'
        form.setLocationRelativeTo(null);
        
        //Setting the visibility of the frame to 'true'
        form.setVisible(true);
    }
    
}
