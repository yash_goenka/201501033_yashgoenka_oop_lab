



package que4stack_gui.GUI;

import javax.swing.*;
import java.awt.*;

public class StackGUI extends JFrame {
    
    //Creating Panels
    JPanel p = new JPanel(new GridLayout(0,2));  
    JPanel button = new JPanel(new FlowLayout(FlowLayout.CENTER)); //Panel for Buttons

    public StackGUI()
    {
        //Setting layout
        setLayout(new GridLayout(2,0));
        
        //Adding components to the panel 'p'
        p.add(new JLabel("Size"));
        p.add(new JTextField(5));
        p.add(new JLabel("Element"));
        p.add(new JTextField(5));
        
        //Adding components to the panel 'button'
        button.add(new JButton("Pop"));
        button.add(new JButton("Push"));
        button.add(new JButton("Peep"));
        
        //Adding panels to the frame
        add(p);
        add(button);
        
        
        
        
        
    }

}
