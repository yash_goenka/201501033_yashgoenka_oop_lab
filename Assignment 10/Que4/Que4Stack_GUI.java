/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package que4stack_gui;

import javax.swing.JFrame;
import que4stack_gui.GUI.StackGUI;
public class Que4Stack_GUI {

    
    public static void main(String[] args) {
        //Creating object of the class 'StackGUI'
        StackGUI Stack = new StackGUI();
        
        //Setting the size of the frame
        Stack.setSize(300,200);
        
        
        //Terminates the progran on closing the window
        Stack.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //Setting the location of the frame to 'Centre'
        Stack.setLocationRelativeTo(null);
        
        //Setting the visibility of the frame to 'true'
        Stack.setVisible(true);
    }
    
}
