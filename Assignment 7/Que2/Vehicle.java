
package vehicle.que2;

import java.util.Scanner;


public abstract class Vehicle  {
    
    String colour,price,type,company,subtype;
    int qty, capacity;
    float topspeed,weight;
    Scanner in = new Scanner(System.in);
    Engine en;
    Vehicle()
    {
        float size=1;
         en= new Engine(size);
    }
    
    Vehicle(float size)
    {
        en= new Engine(size);
    }
    
    public void readInfo()
    {
        System.out.println("Please enter the company");
        company=in.next();
        System.out.println("Please enter the price of the vehicle(in rupees)");
        price=in.next();
        System.out.println("Please enter the quantity of the vehicle");
        qty=in.nextInt();
        System.out.println("Please enter the weight of the vehicle(in KG)");
        weight=in.nextFloat();
        System.out.println("Please enter the topspeed of the vehivle(in KM/HR)");
        topspeed=in.nextFloat();
        System.out.println("Please enter the colour of the object");
        colour=in.next();
        System.out.println("Please enter the capacity(how many persons) of the vehicle");
        capacity=in.nextInt();
               
    }
    public void displayinfo()
    {
        System.out.println("Company: "+ company);
        System.out.println("Price: " + price+" Rs");
        System.out.println("Quantity: " + qty );
        System.out.println("Engine size: "+ en.size+"  litre");
        System.out.println("Colour: "+ colour);
        System.out.println("Weight: "+ weight+" kg");
        System.out.println("Top Speed: "+topspeed+"km/hr");
        System.out.println("Capacity: "+capacity+"persons");
        
    }
}
