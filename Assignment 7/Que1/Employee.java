
package employee;

import java.util.Scanner;

public class Employee {

    
    public static void main(String[] args)
    {
        byte type;
        Scanner in = new Scanner(System.in);
        
        System.out.println("1. Permanent Employee");
        System.out.println("2. Ad hoc Employee");
        System.out.println("Please enter a choice");
        type=in.nextByte();
        Emp e = null;
        
        if(type==1)
            e=new permanentEmp();
        if(type==2)
            e=new adhocEmp();
        
        e.readInfo();
        while(true)
        {System.out.println("1. Show gross salary");
        System.out.println("2. Show net salary");
        System.out.println("3. Show details of the employee");
        System.out.println("0. Exit");
        System.out.println("Please enter a choice");
        byte ch=in.nextByte();
        type = 0;
        if(ch ==1||ch==2)
        {
            System.out.println("Do you want enter new values for Basic Salary, TA, TDS, PT, HRA");
        
        System.out.println("1-Yes\n2-No");
        System.out.println("Please enter a choice");
        
        type=in.nextByte();
        }
        if(type==1)
        {
            e.readval();
            
        }
        if(ch ==1 )
            e.gSalary();
        if(ch==2)
            e.nSalary();
        if(ch ==3)
            e.display();
        if(ch==0)
            return;
        
        }
    }
    
}
