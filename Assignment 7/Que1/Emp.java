package employee;

import java.util.Scanner;


public abstract class Emp {
    
    protected String name, address, contact, dob; double  gsalary, bsalary,nsalary,da,hra,ta,pt,tds,pf;
    
    Scanner in = new Scanner(System.in);
    
    Emp()
    {
        
        bsalary=20000;
        da=0.5*bsalary;
        hra = 0.25*bsalary;
        ta=950;
        pt=200;
        pf=0.1*bsalary;
    }
    public void readInfo()
    {
        System.out.println("Please enter name");
        name=in.next();
        System.out.println("Enter address");
        address=in.next();
        System.out.println("Enter date of birth");
        dob=in.next();
        System.out.println("Enter contact number");
        contact=in.next();
    }
    
    public void readval()
    {
        System.out.println("Enter basic salary");
        bsalary=in.nextDouble();
        System.out.println("Enter  Dearness Allowance(in %)");
        da=(in.nextDouble())/100*bsalary;
        System.out.println("Enter HRA(in %)");
        hra=(in.nextDouble())/100*bsalary;
        System.out.println("Enter Travelling allowance(in Rupees)");
        ta=in.nextDouble();
        System.out.println("Enter Professional Tax(in Rupees)");
        pt=in.nextDouble();
        
        
    }
    
    public abstract void gSalary();
    
    public abstract void nSalary();
    
    public void display()
    {
        System.out.println("Name: "+ name);
        System.out.println("Contact No: "+ contact);
        System.out.println("Address: "+ address);
        System.out.println("Date of Birth: "+ dob);
    }
}
