

package lab9exp2;

import java.util.Scanner;

class Stack
{
    //Data Members
    int [] arraystack;
    int size;
    int top;
    
    Stack(int k)                                    //Constructor to initialise values.
    {
        size = k;
        arraystack = new int[size];
        top = -1;
    }
    
    
    public void pushelements()                //Member function to Push elements into stack.
    {
        Scanner in = new Scanner(System.in);
        try
        {
            this.checknull();
            
            try
            {
                this.checklength(size);               
                
            }

            catch(IndexOutOfBounds err)
            {
                System.out.println("Exception occured!\n" +err);
                System.out.println("\nEnter size: ");
                size=in.nextInt();
            }
            
               
        }
        catch(NullPointer err)
        {
            System.out.println("Exception occured!\n"+err);
        }
        
        int i;


        for(i=0;i<arraystack.length;i++)
        {
            System.out.println("\nEnter value no."+(i+1)+": ");
            arraystack[i] = in.nextInt();
            top++;
        }     
       
    }
    
    
    public void displaynumbers()          //Member function to Display the stack. FOR REFERENCE.
    {
        try
        {
            this.checklength(arraystack.length);
        }            
        
        catch(IndexOutOfBounds err)
        {
            System.out.println("Exception Occured\n"+err);
        }
        
        int i;

        System.out.print("\n(");
        for(i=arraystack.length-1;i>=0;i--)
        {
            if(i>0)
                System.out.print(""+arraystack[i]+", ");

            else
                System.out.print(""+arraystack[i]+")\n");

        }
    }
    
    
    public void popelement()             //Member function to Pop the topmost element. Change the value of top element.
    {
        try
        {
            this.checklength(arraystack.length);
        }
        
        catch(IndexOutOfBounds err)
        {
            System.out.println("Exception Occured\n"+err);
        }
        
        
        System.out.println("The topmost #"+top+" element is: " +arraystack[top]);

        arraystack[top]=0;
        top--;

        System.out.println("\nThe topmost element is removed!\n");

    }
    
    
    public void peepelement()                   //Member function to Display the topmost element.
    {
        try
        {
            this.checklength(arraystack.length);
        }
        
        catch(IndexOutOfBounds err)
        {
            System.out.println("Exception Occured\n"+err);
        }
        
        System.out.println("\n\nThe topmost #"+top+" element is: " +arraystack[top]+"\n");
        
    }
    
    
    public void emptycheck()                           //Member function to Check whether stack is empty or not
    {
        if(top==-1)
            System.out.println("\nThe stack is empty!\n");
        
        else
            System.out.println("\nThe stack is not empty!\n");
    }
    
    
    public void fullcheck()                        //Member function to Check whether stack is full or not
    {
        try
        {
            this.checklength(arraystack.length);
        }
        
        catch(IndexOutOfBounds err)
        {
            System.out.println("Exception occured!\n" +err);
        }
        
            
        if(top==arraystack.length-1)
        System.out.println("\nThe stack is full!\n");

        else
        System.out.println("\nThe stack is not full!\n");
        
    }
    
    public void checklength(int l) throws IndexOutOfBounds
    {
        if(l==0)
            throw new IndexOutOfBounds("Length of array is Zero! Enter elements!\n");
        
    }
    
    public void checknull() throws NullPointer
    {
        if(this==null)
            throw new NullPointer("No Array created!");
    }    
    
    
    
}