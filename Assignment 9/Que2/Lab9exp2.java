


package lab9exp2;

import java.util.Scanner;


class IndexOutOfBounds extends Exception
{
    IndexOutOfBounds(String str)
    {
        super(str);
    }
}

class NullPointer extends Exception
{
    NullPointer(String str)
    {
        super(str);        
    }
            
}



public class Lab9exp2
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        int size, choice;
        System.out.println("Enter the size of the array(stack): ");             //Accepting size of stack.
        size = in.nextInt();
        
        
        Stack newnumbers = new Stack(size);                     //Creating object.
        
        
        
        while(true)
        {
            System.out.println("\n1. Push elements into stack.");
            System.out.println("2. Pop an element from the stack.");
            System.out.println("3. Peep the topmost element.");
            System.out.println("4. Check whether stack is empty or not.");
            System.out.println("5. Check whether stack is full or not.");
            System.out.println("0. Exit");
            System.out.println("\nEnter your choice: ");
            
            choice = in.nextInt();
            
            switch(choice)
            {
                case 1:
                {
                    newnumbers.pushelements();                      //Calling member function to push elements into stack.
                }
                
                break;
                
                case 2:
                {
                    newnumbers.popelement();                        //Calling member function to pop the topmost element.
                    System.out.println("\nThe array is: ");
                    newnumbers.displaynumbers();                    //Calling member function to display stack elements. FOR REFERENCE.
                }
                
                break;
                
                case 3:
                {
                    newnumbers.peepelement();                        //Calling member function to peep into the topmost element.
                }
                
                break;
                
                case 4:
                {
                    newnumbers.emptycheck();                           //Calling member function to check whether stack is empty or not.
                }
                
                break;
                
                case 5:
                {
                    newnumbers.fullcheck();                        //Calling member function to check whether stack is full or not.
                }
                
                break;
                
                case 0: return;
                    
            }
        }
        
    }
    
}
