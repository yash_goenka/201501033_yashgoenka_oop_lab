package lab_9_q5;
import java.util.Scanner;



public class Lab9Exp5 {
        //Creating main function
        public static void main(String[] args) {
                
                int choice1;
                Scanner input = new Scanner(System.in);
                
                //Creating reference of the object
                Account obj1 = null;
                double temp;
                
                System.out.println("Enter 1. if your account type is Savings \nEnter 2. if your account type is Current");
                System.out.print("Enter your choice : ");
                choice1 = input.nextInt();
                
                if(choice1 == 1)
                {       

                        //Handling the Exception
                        try
                        {
                                obj1 = new Savings();
                        }
                        catch(Exception error)
                        {
                                System.out.println(error);
                                System.out.println("\nOperation not performed due to previous errors");
                                return;
                        }
                }
                else if(choice1 == 2)
                {       
                        try
                        {
                                obj1 = new Current();
                        }
                        catch(Exception error)
                        {
                                System.out.println(error);
                                System.out.println("\nOperation not performed due to previous errors");
                                return;
                        }
                                  
                }
                else
                {
                        System.out.println("Invalid choice");
                        return;
                }
                
                while(true)
                {
                        //Displaying the menu
                        System.out.println("Enter 1. to Desposit money");
                        System.out.println("Enter 2. to Withdraw money");
                        System.out.println("Enter 3. to view details");
                        System.out.println("Enter 4. to set overdraft limit");
                        System.out.println("Enter 5. to set rate of interest");
                        System.out.println("Enter 6. to calculate interest rate");
                        System.out.println("Enter 7. to Exit");
                        choice1 = input.nextInt();

                        switch(choice1)
                        {
                                case 1: System.out.print("Enter the Amount to be deposited : ");
                                        temp = input.nextDouble();
                                        obj1.deposit(temp);
                                        break;
                                case 2: System.out.print("\nEnter the Amount to be Withdrawn : ");
                                        temp = input.nextDouble();
                                        obj1.Withdraw(temp);
                                        break;
                                case 3: obj1.view_details();
                                        break;
                                case 4: try
                                        {
                                                if(obj1 instanceof Current)
                                                {
                                                        System.out.println("Enter the amount : ");
                                                        temp = input.nextDouble();
                                                        ((Current) obj1).set_odl(temp);
                                                }
                                                else
                                                {
                                                        throw new Exception("\nInvalid option for Savings Account");
                                                }
                                        }
                                        catch(Exception error)
                                        {
                                                System.out.println(error);
                                                System.out.println("Operation not perfromed due to prevoius errors");
                                        }
                                        break;
                                case 5: try
                                        {
                                                if(obj1 instanceof Savings)
                                                {
                                                        System.out.println("Enter the interest rate : ");
                                                        temp = input.nextDouble();
                                                        ((Savings) obj1).set_roi(temp);
                                                }
                                                else
                                                {
                                                        throw new Exception("\nInvalid option for Current Account");
                                                }
                                        }
                                        catch(Exception error)
                                        {
                                                System.out.println(error);
                                                System.out.println("Operation not perfromed due to prevoius errors");
                                        }
                                case 6: try
                                        {
                                                if(obj1 instanceof Savings)
                                                {
                                                        ((Savings) obj1).calc();
                                                }
                                                else
                                                {
                                                        throw new Exception("\nInvalid option for Current Account");
                                                }
                                        }
                                        catch(Exception error)
                                        {
                                                System.out.println(error);
                                                System.out.println("Operation not perfromed due to prevoius errors");
                                        }
                                        break;                              
                                case 7:
                                        return;
                        }
                }
        }        
}
