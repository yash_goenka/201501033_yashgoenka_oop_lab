package lab_9_q5;

import java.util.Scanner;

//Creating child class of 'Account' class
public class Current extends Account {
        private double odl;
        
        public void set_odl(double amt)
        {
                odl += amt;
        }
        public Current() throws Exception
        {       
                super(0);
                
                Scanner input = new Scanner(System.in);
                
                System.out.print("Enter the Overdraft limit : ");
                odl = input.nextDouble();
                
                //Handling Exception for wrong overdraft limit 
                try
                {
                        if(odl < 500 && odl > 10000000) 
                        {       
                                odl = 0;
                                throw new Exception();
                        }
                }
                catch(Exception error)
                {
                        System.out.println(error);
                        System.out.println("\nOperation not performed due to previous errors");
                }
                          
        }
     
        
        //Overiding abstract method 'Withdraw'
        public void Withdraw(double amount)
        {
                try
                {
                        //Handling Exception for wrong withdraw amount                  
                        if(amount > 0 && amount <= (balance + odl))
                        {
                                balance -= amount;
                        }
                        else
                        {
                                throw new Exception("\nTransition incomplete");
                        }
                }
                catch(Exception error)
                {
                        System.out.println(error);
                        System.out.println("Operation not performed due to previous errors");
                }
        }
        //Overiding abstract method 'view_details'
        public void view_details()
        {
                System.out.println("\nAccount number : " + super.get_acc_no());
                System.out.println("Balance : " + balance);
                System.out.println("Overdraft Limit : " + odl);
        }
}