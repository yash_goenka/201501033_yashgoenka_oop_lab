package lab_9_q5;

import java.util.Scanner;
import java.util.Date;
import java.util.Calendar;
//Creating child class of 'Account' class
public class Savings extends Account 
{
        private double rate_of_int;
        private int year;
        private int month;
                
        public Savings() throws Exception
        {
                super(1000);
                
                Scanner input = new Scanner(System.in);
                
                System.out.print("\nEnter the rate of interest : ");
                rate_of_int = input.nextByte();
                
                System.out.print("\nEnter current year : ");
                year = input.nextInt();
                
                System.out.print("Enter current month : ");
                month = input.nextInt();
        }
        //Overiding 'Withdraw' function
        public void Withdraw(double amount)
        {
                //Handling Exception for wrong withdraw amount
                try
                {
                        if(amount > 0 && amount < balance - 500)
                        {
                                balance -= amount;
                        }
                        else
                        {
                                throw new Exception("\nTransition incomplete");
                        }
                }
                catch(Exception error)
                {
                        System.out.println(error);
                        System.out.println("Operation not performed due to previous errors");
                }
        }
        public double get_roi()
        {
                return rate_of_int;
        }
        public void calc() throws Exception
        {
                Scanner input = new Scanner(System.in);
                int temp1,temp2;
                
                System.out.print("Enter current year : ");
                temp1 = input.nextInt();
                System.out.print("Enter current month : ");
                temp2 = input.nextInt();                
                
                try 
                {
                        if((temp1 > 2015) && (temp2 > 0 && temp2 <= 12))
                        {
                                if((temp2 - month) == 0)
                                {
                                        balance += 4 * (temp1 - year) * rate_of_int * balance / 100;
                                }
                                else
                                {
                                        balance += 4 * (temp1 - year - 1) * rate_of_int * balance / 100;
                                }
                        }
                        else
                        {
                                throw new Exception();
                        }
                }
                catch(Exception error)
                {
                        throw new Exception();
                }                       
                
   
      
        }
        //Overiding 'view_details'
        public void view_details()
        {
                System.out.println("\nAccount number : " + super.get_acc_no());
                System.out.println("Balance : " + balance);
                System.out.println("Rate of Interest : " + rate_of_int);
        }
        public void set_roi(double amount)
        {
                rate_of_int = amount;
        }
}
