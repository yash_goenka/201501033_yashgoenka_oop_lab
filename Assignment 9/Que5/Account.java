package lab_9_q5;

import java.util.Scanner;


//Creating parent class account
public abstract class Account {
        private String acc_no;
        protected double balance;

        public Account(int x) throws Exception
        {
                Scanner input = new Scanner(System.in);
                //Handling Exception for invalid account number
                try
                {
                        System.out.print("\nEnter your account number : ");
                        acc_no = input.next();
                        balance = x;
                        
                        if(acc_no.length() != 12)
                        {
                                throw new Exception("Invalid bank account number");
                        }
                }
                catch(Exception error)
                {
                        throw error;
                }
        }
        
        public void deposit(double amount)
        {
                //Handling Exception for invalid amount
                try
                {
                        if(amount > 0)
                        {
                                balance += amount;
                        }
                        else
                        {
                                throw new Exception("Invalid Amount");
                        }
                }
                catch(Exception error)
                {
                        System.out.println(error);
                        System.out.println("Operation not performed due to previous errors");
                }
        }
        
        
        //Abstract funtions for withdraw and displaying details 
        public abstract void Withdraw(double amount);
        public abstract void view_details();
        
        public String get_acc_no()
        {
                return acc_no;                
        }
        public double get_balance()
        {
                return balance;
        }
}
