



package lab9exp3;

import java.util.Scanner;

class IndexOutOfBounds extends Exception
{
    private int r, c;
    IndexOutOfBounds(String s, int x, int y)
    {
        super(s);
        r = x;
        c = y;
    }
}

class NullPointer extends Exception
{
    NullPointer(String s)
    {
        super(s);        
    }
            
}


public class Lab9exp3
{

    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);
        int rows, cols, rows2, cols2, cnt;
        byte check;
        byte ch=0;
        int constant;
        boolean mat;
        
        System.out.println("Enter number of ROWS for Matrix 1:");
        rows = in.nextInt();
        
        System.out.println("Enter number of COLUMNS for Matrix 1:");
        cols = in.nextInt();
        
        Matrix M1 = new Matrix(rows, cols);                 //Creating Matrix1(new object)
        Matrix M2;                                      //Creating Matrix2(new reference)
        
        
        System.out.print("\n\n");
        
        while(true)
        {
            System.out.println("1. Enter values in matrix.");
            System.out.println("2. Display values of the matrix.");
            System.out.println("3. Display values as string.");
            System.out.println("4. Find the transpose of the matrix.");
            System.out.println("5. Add two matrices.");
            System.out.println("6. Multiply two matrices.");
            System.out.println("7. Multiply a matrix with a scalar.");
            System.out.println("8. Check if the matrix is a magic square.");
            System.out.println("9. Display copied matrix using constructor.");
            System.out.println("0. Exit.");
            System.out.println("Enter your choice: ");
            ch= in.nextByte();
            
            switch(ch)
            {
                case 1: 
                {
                    try
                    {
                        M1.readMatrix();                    //Calling Member Function to insert values.
                    }
                    catch(IndexOutOfBounds err)
                    {
                        System.out.println("\nException occured!\n" +err+"\n\nRows: "+M1.rows + " and Columns: "+M1.cols+"\n");
                        Lab9exp3.main(args);
                    }
                        
                                                       
                }
                
                break;
                
                case 2:
                {
                    check=0;
                    
                    try
                    {
                        check=M1.checkrowscols();           //Calling Member Function to check whether matrix is empty or not.
                    }
                    catch(IndexOutOfBounds err)
                    {
                        System.out.println("\nException occured!\n" +err+"\n\nRows: "+M1.rows + " and Columns: "+M1.cols+"\n");
                        Lab9exp3.main(args);
                    }

                    if(check==1)
                    {
                        System.out.println("\nThe matrix entered is: \n");
                        M1.displayMatrix();                                   //Calling Member Function to display the matrix..
                    }
                        
                    
                    else if(check==0)
                    {
                        try
                        {
                            M1.readMatrix();                                                //Calling Member Function to insert values.
                        }
                        
                        catch(IndexOutOfBounds err)
                        {
                            System.out.println("\nException occured!\n" +err+"\n\nRows: "+M1.rows + " and Columns: "+M1.cols+"\n");
                            Lab9exp3.main(args);
                        }
                        
                        M1.displayMatrix();                                    //Calling Member Function to display the matrix.
                    }
                }
                break;
                
                case 3:
                {
                    try
                    {
                        check=M1.checkrowscols();           //Calling Member Function to check whether matrix is empty or not.
                    }
                    
                    catch(IndexOutOfBounds err)
                    {
                        System.out.println("\nException occured!\n" +err+"\n\nRows: "+M1.rows + " and Columns: "+M1.cols+"\n");
                        Lab9exp3.main(args);
                    }
                    
                    M1.toString();                                    //Calling Member Function to convert the values to string.
                    
                    System.out.println("\nThe matrix entered is: \n" + M1);
                }
                
                break;
                
                case 4:
                {
                    check=0;
                    try
                    {
                        check=M1.checkrowscols();           //Calling Member Function to check whether matrix is empty or not.
                    }
                    catch(IndexOutOfBounds err)
                    {
                        System.out.println("\nException occured!\n" +err+"\n\nRows: "+M1.rows + " and Columns: "+M1.cols+"\n");
                        Lab9exp3.main(args);
                    }

                    if(check==1)
                    {
                        System.out.println("\nThe matrix entered is: \n");
                        M1.displayMatrix();                                   //Calling Member Function to display the matrix.
                        M1.transpose();                                    //Calling Member Function to find tranpose of the matrix.
                    }
                    
                    else if(check==0)
                    {
                        System.out.println("\nEnter matrix 1 here: \n");
                        try
                        {
                            M1.readMatrix();                    //Calling Member Function to insert values.
                        }
                        catch(IndexOutOfBounds err)
                        {
                            System.out.println("\nException occured!\n" +err+"\n\nRows: "+M1.rows + " and Columns: "+M1.cols+"\n");
                            Lab9exp3.main(args);
                        }
                        
                        M1.displayMatrix();
                        M1.transpose();                                    //Calling Member Function to find tranpose of the matrix.
                    }
                }
                break;
                
                case 5:
                {
                    check =0;
                    try
                    {
                        check=M1.checkrowscols();           //Calling Member Function to check whether matrix is empty or not.
                    }
                    catch(IndexOutOfBounds err)
                    {
                        System.out.println("\nException occured!\n" +err+"\n\nRows: "+M1.rows + " and Columns: "+M1.cols+"\n");
                        Lab9exp3.main(args);
                    }
                    
                    System.out.println("\nNOTE: For addition, the rows and columns of both matrices must be same!!\n");
                    System.out.println("Enter number of ROWS for Matrix 2: ");
                    rows2 = in.nextInt();
                    System.out.println("Enter number of COLUMNS for Matrix 2: ");
                    cols2 = in.nextInt();
                    M2 = new Matrix(rows2, cols2);                      //Initialising the object.
                    
                    try
                    {
                        M2.readMatrix();                                    //Calling Member Function to insert values.
                    }
                    catch(IndexOutOfBounds err)
                    {
                        System.out.println("\nException occured!\n" +err+"\n\nRows: "+M2.rows + " and Columns: "+M2.cols+"\n");
                        System.out.println("Enter number of ROWS again for Matrix 2: ");
                        rows2 = in.nextInt();
                        System.out.println("Enter number of COLUMNS again for Matrix 2: ");
                        cols2 = in.nextInt();
                        M2 = new Matrix(rows2, cols2);                      //Initialising the object.
                        try
                        {
                            M2.readMatrix();                                    //Calling Member Function to insert values.
                        }
                        catch(IndexOutOfBounds err2)
                        {
                            System.out.println("\nException!");                            
                        }
                            
                    }
                   
                    if(check==1)
                    {
                        System.out.println("\nThe matrix entered is: \n");
                        M1.displayMatrix();                                   //Calling Member Function to display the matrix..
                        M1.add(M2);                                    //Calling Member Function to add two matrices.
                    }
                    
                    else if(check==0)
                    {
                        System.out.println("\nEnter matrix 1 here: \n");
                        try
                        {
                            M1.readMatrix();                                    //Calling Member Function to insert values.
                        }
                        catch(IndexOutOfBounds err)
                        {
                            System.out.println("\nException occured!\n" +err+"\n\nRows: "+M1.rows + " and Columns: "+M1.cols+"\n");
                            Lab9exp3.main(args);
                        }
                        
                        M1.add(M2);                                    //Calling Member Function to add two matrices.
                    }
                    
                }
                
                break;
                
                case 6:
                {
                    check =0;
                    try
                    {
                        check=M1.checkrowscols();           //Calling Member Function to check whether matrix is empty or not.
                    }
                    catch(IndexOutOfBounds err)
                    {
                        System.out.println("\nException occured!\n" +err+"\n\nRows: "+M1.rows + " and Columns: "+M1.cols+"\n");
                        Lab9exp3.main(args);
                    }
                    
                    System.out.println("\nNOTE: For multiplication, the columns of first matrix and rows of second matrix must be same!!\n");
                    System.out.println("Enter number of ROWS for Matrix 2: ");
                    rows2 = in.nextInt();
                    System.out.println("Enter number of COLUMNS for Matrix 2: ");
                    cols2 = in.nextInt();
                    M2 = new Matrix(rows2, cols2);                  //Initialising the object.
                    
                    try
                    {
                        M2.readMatrix();                                    //Calling Member Function to insert values.
                    }
                    catch(IndexOutOfBounds err)
                    {
                        System.out.println("\nException occured!\n" +err+"\n\nRows: "+M2.rows + " and Columns: "+M2.cols+"\n");
                        System.out.println("\nEnter number of ROWS again for Matrix 2: ");
                        rows2 = in.nextInt();
                        System.out.println("\nEnter number of COLUMNS again for Matrix 2: ");
                        cols2 = in.nextInt();
                       
                        M2 = new Matrix(rows2, cols2);                      //Initialising the object.
                        try
                        {
                            M2.readMatrix();                                    //Calling Member Function to insert values.
                        }
                        catch(IndexOutOfBounds err2)
                        {
                            System.out.println("\nException!");                            
                        }
                    }
                    
                    
                    if(check==1)
                    {
                        System.out.println("The matrix entered is: \n");
                        M1.displayMatrix();                                   //Calling Member Function to display the matrix.
                        M1.multiply(M2);                                    //Calling Member Function to multiply two matrices.
                    }
                    
                    else if(check==0)
                    {
                        System.out.println("\nEnter matrix 1 here: \n");
                        
                        try
                        {
                            M1.readMatrix();                    //Calling Member Function to insert values.
                        }
                        catch(IndexOutOfBounds err)
                        {
                            System.out.println("\nException occured!\n" +err+"\n\nRows: "+M1.rows + " and Columns: "+M1.cols+"\n");
                            Lab9exp3.main(args);
                        }
                        
                        M1.multiply(M2);                                    //Calling Member Function to multiply two matrices.
                    }
                    
                }
                
                break;
                
                case 7:
                {
                    try
                    {
                        check=M1.checkrowscols();           //Calling Member Function to check whether matrix is empty or not.
                    }
                    catch(IndexOutOfBounds err)
                    {
                        System.out.println("\nException occured!\n" +err+"\n\nRows: "+rows + " and Columns: "+cols);
                        Lab9exp3.main(args);
                    }
                    
                    System.out.println("Enter the constant: ");
                    constant = in.nextInt();
                    
                    check=0;
                    

                    if(check==1)
                    {
                        System.out.println("The matrix entered is: \n");
                        M1.displayMatrix();                                         //Calling Member Function to display the matrix.
                        M1.multiply(constant);                                               //Calling Member Function to multiply a constant with the matrix.
                        System.out.println("\nThe matrix after multiplication is: \n");
                        M1.displayMatrix();                                         //Calling Member Function to display the matrix.
                    }
                   
                    else if(check==0)
                    {
                        try
                        {
                            M1.readMatrix();                    //Calling Member Function to insert values.
                        }
                        catch(IndexOutOfBounds err)
                        {
                            System.out.println("\nException occured!\n" +err+"\n\nRows: "+M1.rows + " and Columns: "+M1.cols+"\n");
                            Lab9exp3.main(args);
                        }
                        M1.multiply(constant);                                    //Calling Member Function to multiply a constant with the matrix.
                        System.out.println("\nThe matrix after multiplication is: \n");
                        M1.displayMatrix();                                    //Calling Member Function to display the matrix.
                    }
                    
                }
                break;
                
                case 8:
                {
                    check=0;
                    try
                    {
                        check=M1.checkrowscols();           //Calling Member Function to check whether matrix is empty or not.
                    }
                    catch(IndexOutOfBounds err)
                    {
                        System.out.println("\nException occured!\n" +err+"\n\nRows: "+M1.rows + " and Columns: "+M1.cols+"\n");
                        Lab9exp3.main(args);
                    }

                    if(check==1)
                    {
                        System.out.println("The matrix entered is: \n");
                        M1.displayMatrix();                                   //Calling Member Function to display the matrix.
                        mat=M1.checkelements();                                    //Calling Member Function to check whether matrix is valid to be magic square.
                        if(mat)
                            M1.magicsquare();                                    //Calling Member Function to check whether matrix is a magic square or not.
                        
                       else if(mat==false)
                            System.out.println("The matrix is not a magic square!\n");
                    }
                    
                        
                    
                    else if(check==0)
                    {
                        System.out.println("\nEnter matrix 1 here: \n");
                        try
                        {
                            M1.readMatrix();                    //Calling Member Function to insert values.
                        }
                        catch(IndexOutOfBounds err)
                        {
                            System.out.println("\nException occured!\n" +err+"\n\nRows: "+M1.rows + " and Columns: "+M1.cols+"\n");
                            Lab9exp3.main(args);
                        }
                        
                        mat=M1.checkelements();                                    //Calling Member Function to check whether matrix is valid to be magic square.
                        if(mat==true)
                            M1.magicsquare();                                    //Calling Member Function to check whether matrix is a magic square or not.
                        
                        else if(mat==false)
                            System.out.println("The matrix is not a magic square!\n");
                    }
                    
                }
                break;
                
                case 9:
                {
                    try
                    {
                        M1.readMatrix();                    //Calling Member Function to insert values.
                    }
                    catch(IndexOutOfBounds err)
                    {
                        System.out.println("\nException occured!\n" +err+"\n\nRows: "+M1.rows + " and Columns: "+M1.cols+"\n");
                        Lab9exp3.main(args);
                    }
                    
                    Matrix copy = new Matrix(M1);                                    //Calling Copy Constructor to create a copy of the object.
                    System.out.println("The copied matrix is: \n");
                    copy.displayMatrix();                                    //Calling Member Function(of Copied Object) to display the matrix.
                  
                }
                break;
                
                case 0: return;                             //To exit the loop.
                    
                     
                    
            }
        }
        
    }
    
}

