

package lab9exp3;

import java.util.Scanner;

class Matrix
{
    protected int rows;
    protected int cols;
    private int [][] matrix;
    
    Matrix(int k, int j)                //Constructor to inititalise values.
    {
        rows = k;
        cols = j;
        matrix = new int[rows][cols];
        
    }
    
    Matrix(Matrix cur)                             //Copy Constructor to create another object with same values.
    {
        
                
        int i, j;
        rows=cur.rows;
        cols = cur.cols;
        matrix = new int[rows][cols];
        
        for(i=0;i<rows;i++)
        {
            for(j=0;j<cols;j++)
            {
                matrix[i][j]=cur.matrix[i][j];
            }
        }
    }
    
    public void readMatrix() throws IndexOutOfBounds                             //Member Function to insert values.
    {        
        try
        {
            if(this==null)
                throw new NullPointer("\nNo Matrix created!");
        }
        
        catch(NullPointer err)
        {
            System.out.println("Exception occured!\n"+err);
        }
        
        try
        {
            if(rows==0 || cols==0)
                throw new IndexOutOfBounds("Rows or Columns CANNOT be zero!\n", rows, cols);
        }
        
        catch(IndexOutOfBounds err)
        {
            throw err;
        }
        
        try
        {
            if(rows<0 || cols <0 )
                throw new IndexOutOfBounds("Rows or Columns CANNOT be negative!\n", rows, cols);
        }
        catch(IndexOutOfBounds err)
        {
            throw err;
        }
        
        
        Scanner in = new Scanner(System.in);
        int i, j;
        
        
        for(i=0;i<rows;i++)
        {
            System.out.println("\n");
            System.out.print("For Row #"+(i+1)+" : \n");
            
            for(j=0;j<cols;j++)
            {
                System.out.println("Enter element ["+(i+1)+"]["+(j+1)+"]: ");
                matrix[i][j]= in.nextInt();
            }
        }
        System.out.print("\n\n");
        
    }
    
    public void displayMatrix()                            //Member Function to display the matrix.
    {
        try
        {
            if(this==null)
                throw new NullPointer("\nNo Matrix created!");
        }
        
        catch(NullPointer err)
        {
            System.out.println("Exception occured!\n"+err);
        }
        
        try
        {
            if(rows==0 || cols==0)
                throw new IndexOutOfBounds("Rows or Columns CANNOT be zero!\n", rows, cols);
        }
        
        catch(IndexOutOfBounds err)
        {
            System.out.println("\nException occured!\n" +err+"\n\nRows: "+rows + " and Columns: "+cols);
        }
        
        try
        {
            if(rows<0 || cols <0 )
                throw new IndexOutOfBounds("Rows or Columns CANNOT be negative!\n", rows, cols);
        }
        
        catch(IndexOutOfBounds err)
        {
            System.out.println("\nException occured!\n" +err+"\n\nRows: "+rows + " and Columns: "+cols);
        }
        
        int i, j;
        System.out.print("\n");
        for(i=0;i<rows;i++)
        {
            for(j=0;j<cols;j++)
            {
                System.out.print(+matrix[i][j]);
                System.out.print("\t");
            }
            
            System.out.print("\n");
        }
        System.out.print("\n");
        
    }
    
    @Override
    public String toString()                                //Member Function to convert the values into string.
    {
        try
        {
            if(this==null)
                throw new NullPointer("\nNo Matrix created!");
        }
        
        catch(NullPointer err)
        {
            System.out.println("Exception occured!\n"+err);
        }
        
        
        
        String displayM = new String();
        int i, j;
        
        displayM+="\n\n";
        
        for(i=0;i<rows;i++)
        {
            for(j=0;j<cols;j++)
            {
                displayM+=matrix[i][j];
                displayM+="\t";
            }
            
            displayM+="\n";
        }
        
        return displayM;
    }
    
    public byte checkrowscols() throws IndexOutOfBounds                               //Member Function to check whether matrix is empty or not.
    {
        try
        {
            if(this==null)
                throw new NullPointer("\nNo Matrix created!");
        }
        
        catch(NullPointer err)
        {
            System.out.println("Exception occured!\n"+err);
        }
        
        try
        {
            if(rows==0 || cols==0)
                throw new IndexOutOfBounds("Rows or Columns CANNOT be zero!\n", rows, cols);
        }
        
        catch(IndexOutOfBounds err)
        {
            throw err;
        }
        
        try
        {
            if(rows<0 || cols <0 )
                throw new IndexOutOfBounds("Rows or Columns CANNOT be negative!\n", rows, cols);
        }
        catch(IndexOutOfBounds err)
        {
            throw err;
        }
        
        
        int i, j, cnt=0;
        for(i=0;i<rows;i++)
        {
            for(j=0;j<cols;j++)
            {
                if(matrix[i][j]==0)
                    cnt++;
            }
        }
        
        if(cnt==(rows*cols))
        {
            System.out.println("\nThe matrix is empty! Enter a matrix first!");
        
            return 0;
        }
        
        else
            return 1;
            
            
    }
        
    
    public void transpose()                                //Member Function to find transpose of matrix.
    {
        try
        {
            if(this==null)
                throw new NullPointer("\nNo Matrix created!");
        }
        
        catch(NullPointer err)
        {
            System.out.println("Exception occured!\n"+err);
        }
        
        
        
        int[][] transpose = new int[rows][cols];
        
        int i, j = 0;
        
        for(i=0;i<rows;i++)
        {
            for(j=0;j<cols;j++)
            {
                if(i==j)
                {
                    transpose[i][j]=matrix[i][j];
                }
                
                else if(i!=j)
                {
                    transpose[j][i]=matrix[i][j];
                }
                    
            }
        }
        
        System.out.println("\nThe tranpose of the matrix is: \n");
        for(i=0;i<rows;i++)
        {
            for(j=0;j<cols;j++)
            {
                System.out.print(+transpose[i][j]);
                System.out.print("\t");
            }
            
            System.out.print("\n");
        }
        System.out.print("\n");
        
        
    }
    
    
    public void add(Matrix M2)                                //Member Function to add two matrices.
    {
        try
        {
            if(this==null)
                throw new NullPointer("\nNo Matrix created!");
        }
        
        catch(NullPointer err)
        {
            System.out.println("Exception occured!\n"+err);
        }
        
        try
        {
            if(M2==null)
                throw new NullPointer("\nNo Matrix created!");
        }
        
        catch(NullPointer err2)
        {
            System.out.println("Exception occured!\n"+err2);
        }
        
        int i, j;
        
        
        if(M2.rows!=rows || M2.cols!=cols)
        {
            System.out.println("\nBoth the matrices must have the same rows and columns !!\n");
        }
        
        else if(M2.rows== rows && M2.cols == cols)
        {
            int[][] addmatrix = new int[rows][cols];
            
            for(i=0;i<rows;i++)
            {
                for(j=0;j<cols;j++)
                {
                    addmatrix[i][j] = matrix[i][j]+M2.matrix[i][j];
                }
            }
            System.out.println("\nThe result of the addition of the two matrices is: ");
            
            for(i=0;i<rows;i++)
            {
                for(j=0;j<cols;j++)
                {
                    System.out.print(+addmatrix[i][j]);
                    System.out.print("\t");
                }

                System.out.print("\n");
            }
            System.out.print("\n");
            
        }
    }
    
    public void multiply(Matrix M2)                                //Member Function to multiply two matrices.
    {
        
        try
        {
            if(this==null)
                throw new NullPointer("\nNo Matrix created!");
        }
        
        catch(NullPointer err)
        {
            System.out.println("Exception occured!\n"+err);
        }
        
        try
        {
            if(M2==null)
                throw new NullPointer("\nNo Matrix created!");
        }
        
        catch(NullPointer err2)
        {
            System.out.println("Exception occured!\n"+err2);
        }
        
        int i, j, k;
        if(M2.rows!=cols)
        {
            System.out.println("The matrices cannot be multiplied with each other !");
            System.out.println("\nEnter the rows of matrix 2 as the same as columns of matrix 1!");            
        }
        
        else if(M2.rows==cols)
        {
            int[][] multiplymatrix =  new int[rows][M2.cols];
            
            for(i=0;i<rows;i++)
            {
                for(j=0;j<M2.cols;j++)
                {
                    for(k=0;k<cols;k++)
                    {
                        multiplymatrix[i][j]=multiplymatrix[i][j]+ matrix[i][k]*M2.matrix[k][j];
                    }
                    
                    
                }
            }
            
            System.out.println("\nThe result of the multiplication of the two matrices is: ");
            
            for(i=0;i<rows;i++)
            {
                for(j=0;j<M2.cols;j++)
                {
                    System.out.print(+multiplymatrix[i][j]);
                    System.out.print("\t");
                }

                System.out.print("\n");
            }
            System.out.print("\n");
            
        }
            
        
    }
    
    public void multiply(int constant)                                //Member Function to multiply constant with a matrix.
    {
        int i, j;
        
        for(i=0;i<rows;i++)
        {
            for(j=0;j<cols;j++)
            {
                matrix[i][j]=constant*matrix[i][j];
            }
        }
    }
    
    public boolean checkelements()                                //Member Function to check elements for validity to check for magic square.
    {
        int i, j, k, cnt=0, flag=0;
        
        for(i=0;i<rows;i++)
        {
            for(j=0;j<cols;j++)
            {
                if(matrix[i][j]>rows*rows)
                    return false;
                else
                    cnt++;
                
            }
        }
        
        if(cnt==(rows*cols))
            flag++;
        
        cnt=0;
        for(k=0;k<rows;k++)
        {
            for(i=0;i<rows && i!=k;i++)
            {
                for(j=0;j<cols;j++)
                {
                    if(matrix[k][j]==matrix[i][j])
                        return false;
                    else
                        cnt++;

                }
            }
        }
        
        if(cnt==(rows*cols))
            flag++;
        
        if(flag==2)
            return true;
        
        else
            return false;
                
        
    }
    
    
    public void magicsquare()                                //Member Function to check whether matrix is a magic square.
    {
        if(rows!=cols)
            System.out.println("\nThe matrix is not a perfect square!");
        
        else if(rows==cols)
        {
            int row1sum=0, row2sum=0, col1sum=0, col2sum=0, row3sum=0, col3sum=0, diag1sum=0, diag2sum=0, actualsum=0;
            int i, j;
            actualsum=rows*((rows*rows)+1)/2;
            
            System.out.println("\nThe actual sum of a magic square of " +rows+"x"+cols+" dimenstion will be: "+actualsum);
            System.out.println("\n\n");
            
            for(j=0;j<cols;j++)
            {
                row1sum+=matrix[0][j];
            }
            
            if(row1sum!=actualsum)
                System.out.println("The matrix is not a magic square!");
            
            else
            {
                for(j=0;j<cols;j++)
                {
                    row2sum+=matrix[1][j];
                }
                
                if(row2sum!=actualsum)
                    System.out.println("The matrix is not a magic square!");
                
                else
                {
                    for(j=0;j<cols;j++)
                    {
                        row3sum+=matrix[2][j];
                    }

                    if(row3sum!=actualsum)
                        System.out.println("The matrix is not a magic square!");
                    
                    else
                    {
                        for(i=0;i<rows;i++)
                        {
                            col1sum+=matrix[i][0];
                        }

                        if(col1sum!=actualsum)
                            System.out.println("The matrix is not a magic square!");
                        
                        else
                        {
                            for(i=0;i<rows;i++)
                            {
                                col2sum+=matrix[i][1];
                            }

                            if(col2sum!=actualsum)
                                System.out.println("The matrix is not a magic square!");
                            
                            else
                            {
                                for(i=0;i<rows;i++)
                                {
                                    col3sum+=matrix[i][2];
                                }

                                if(col3sum!=actualsum)
                                    System.out.println("The matrix is not a magic square!");
                                
                                else
                                {
                                    for(i=0, j=i;i<rows;i++, j++)
                                    {
                                        diag1sum+=matrix[i][j];
                                    }
                                    
                                    if(diag1sum!=actualsum)
                                        System.out.println("The matrix is not a magic square!");
                                    
                                    else
                                    {
                                        for(i=2, j=0; i>=0 && j<cols; i--, j++)
                                        {
                                            diag2sum+=matrix[i][j];
                                        }
                                        
                                        if(diag1sum!=actualsum)
                                            System.out.println("The matrix is not a magic square!");
                                        
                                        else
                                            System.out.println("\nThe given matrix IS A MAGIC SQUARE!\n\n");
                                        
                                        
                                    }
                                       
                                }
                            }
                            
                        }
                    
                    }
                    
                }
                
                
            }
            
        }
    }
  
}

