
package lab9exp4;


import java.util.*;
import java.lang.Math;
import static java.lang.Math.pow;

class Vector
{
    int[] vector = new int[3];
    int dim;
   
    
    Vector(int i)               //Constructor to  initialise values.
    {
        int j;
        dim = i;
        for(j=0;j<3;j++)
        {
            vector[j]=0;
        }
        
    }
    
    public void assign()        //Member function to assign values.
    {
        
        try
        {
            if(this==null)
                throw new NullPointer("No Matrix Created!");
        }
        
        catch(NullPointer err)
        {
            System.out.println("Exception occured!\n"+err);
        }
        
        
        Scanner in = new Scanner(System.in);
        int i;
        
        for(i=0;i<dim;i++)
        {
            System.out.println("Enter coordinate[" +(i+1)+"] : ");
            vector[i]=in.nextInt();
        }
    }
    
    public void checkdim() throws IndexOutOfBounds
    {
        try
        {
            if(dim==0)
                throw new IndexOutOfBounds("Dimension CANNOT be zero!", dim);
        }
        
        catch(IndexOutOfBounds err)
        {
            throw err;
        }
        
        try
        {
            if(dim<0)
                throw new IndexOutOfBounds("Dimension CANNOT be negative!", dim);
        }
        
        catch(IndexOutOfBounds err)
        {
            throw err;
        }
        
    }
            
    
    public void display()        //Member function to display values.
    {
        int i;
        System.out.print("\n(");
        
        if(dim==0)
            System.out.println("Enter valid dimensions!!");
        else
        {
            for(i=0;i<dim;i++)
            {
                if(i<dim-1)
                    System.out.print(""+vector[i]+", ");

                else if(i==dim-1)
                    System.out.print(""+vector[i]+")");
            }
            System.out.print("\n");
        }
            
        
        
    }
    
    public void update()                        //Member function to update values.        
    {
        Scanner in = new Scanner(System.in);
        int i;
        System.out.println("The dimenstions of this vector are: "+this.dim+"\n");
        
        for(i=0;i<dim;i++)
        {
            System.out.println("Enter new coordinate[" +(i+1)+"] : ");
            vector[i]=in.nextInt();
        }
    }
    
    public void magnitude()                     //Member function to calculate magnitude.
    {
        int i;
        float magnitude=0;
        
        for(i=0;i<dim;i++)
        {
            magnitude+= pow(vector[i], 2);
        }
        magnitude= (float)pow(magnitude, 0.5);
        
        System.out.println("\nThe magnitude of the vector is: "+magnitude+"\n");
    }
    
    public void add(Vector V2)                          //Member function to add two vectors.
    {
        int i, dimadd=0;
        
        if(dim>V2.dim)
            dimadd=dim;
        else if(dim<=V2.dim)
            dimadd=V2.dim;
        
        
        int[] addition = new int[dimadd];
        
        for(i=0;i<dimadd;i++)
        {
            addition[i]=vector[i]+V2.vector[i];
        }
        
        System.out.print("\n\nThe addition is: \n\n(");
        
        for(i=0;i<dimadd;i++)
        {
            if(i<dimadd-1)
                System.out.print(""+addition[i]+", ");
            
            else if(i==dim-1)
                System.out.print(""+addition[i]+")");
        }
        System.out.println("\n");
       
    }
    
    public void subtract(Vector V2)                         //Member function to subtract two vectors
    {
        int i, dimsub=0;
        
        if(dim>V2.dim)
            dimsub=dim;
        else if(dim<=V2.dim)
            dimsub=V2.dim;
        
        
        int[] subtraction = new int[dimsub];
        
        for(i=0;i<dimsub;i++)
        {
            subtraction[i]=vector[i]-V2.vector[i];
        }
        
        System.out.print("\nThe subtraction is: \n\n(");
        
        for(i=0;i<dimsub;i++)
        {
            if(i<dimsub-1)
                System.out.print(""+subtraction[i]+", ");
            
            else if(i==dim-1)
                System.out.print(""+subtraction[i]+")");
        }
        System.out.println("\n");
    }
    
    public void scalarprod(Vector V2)                       //Member function to find scalar product of two vectors.
    {
        int i, dimscal=0, scalar = 0;
        
        if(dim>V2.dim)
            dimscal=dim;
        else if(dim<=V2.dim)
            dimscal=V2.dim;
        
        for(i=0;i<dimscal;i++)
        {
            scalar +=vector[i]*V2.vector[i];
        }
        
        System.out.print("\nThe scalar product is: "+scalar+"\n\n");
      
    }
    
    public void vectorprod(Vector V2)                   //Member function to find vector product of two vectors.
    {
        int i;
       
        int [] multiplyprod=new int[3];
        
        for(i=0;i<3;i++)
        {
            multiplyprod[i]=0;
        }
        
        multiplyprod[0]=vector[1]*V2.vector[2] - vector[2]*V2.vector[1];
        multiplyprod[1]=-(vector[0]*V2.vector[2] - vector[2]*V2.vector[0]);
        multiplyprod[2]=vector[0]*V2.vector[1] - vector[1]*V2.vector[0];
        
        System.out.print("The vector product is: \n(");
        for(i=0;i<3;i++)
            {
                if(i<3-1)
                    System.out.print(""+multiplyprod[i]+", ");

                else if(i==3-1)
                    System.out.print(""+multiplyprod[i]+")");
            }
            System.out.print("\n");
        
        
        
    }
    
    public void multiply(int constant)              //Member function to multiply a constant with the vector.
    {
        int i;
        
        for(i=0;i<dim;i++)
        {
            vector[i] =  vector[i]*constant;
        }
        
        System.out.println("The new vector is: \n");
        display();
       
    }
    
    public void polar()                     //Member function to find the polar coordinates of the vector.
    {
        double r=0, theta, phi=0;
        int i;
        
        for(i=0;i<dim;i++)
        {
            r+= pow(vector[i], 2);
        }
        r = (float)pow(r, 0.5);
            
        if(r!=0)
        {
            theta = (vector[2]/r);
 
            r = r*10000;
            r =  Math.floor(r);
            r=r/10000;
            theta = Math.acos(vector[2]/r);
            theta= theta*10000;
            theta =  Math.floor(theta);
            theta=theta/10000;
            
            System.out.println("\nPolar coordinates(r, Î¸, Ï†) are: \n");
           
            if(vector[0]!=0)
            {
                phi = (Math.tan(vector[1]/vector[0]));
                
                

                phi = phi*10000;
                phi =  Math.floor(phi);
                phi = phi/10000;

                System.out.println("("+r+", "+theta+", "+phi+")");
            }
            
            else
                System.out.println("("+r+", "+theta+", 0)");
                
            }   
         
       else
            System.out.println("\nPolar coordinate conversion is not possible. Possibly, vector is at the origin.\n");
       
    }
   
    
}
