
package lab9exp4;

import java.util.Scanner;

class IndexOutOfBounds extends Exception
{
    protected int r;
    IndexOutOfBounds(String str, int x)
    {
        super(str);
        r = x;
    }
}

class NullPointer extends Exception
{
    NullPointer(String str)
    {
        super(str);        
    }
            
}

public class Lab9exp4
{
    public static void main(String[] args) 
    {
        int dim1, dim2, addnew=0, constant;
        byte ch=0, choice=0;
        
        Scanner in = new Scanner(System.in);
        
        System.out.println("\nEnter the dimensions of first vector [MUST BE LESS THAN OR EQUAL TO 3]: ");
        dim1 = in.nextInt();
        
        Vector V1 = new Vector(dim1);                                //Creating Vector1(new object)
        
        
        try
        {
            V1.checkdim();
        }
        
        catch(IndexOutOfBounds err)
        {
            System.out.println("\nException occured!\n\n"+err+"\n\nDimension: "+err.r);
            Lab9exp4.main(args);
        }
        
                
        System.out.println("\nEnter for first Vector: \n");
        V1.assign();                                                    //Calling Member function to assign values.
        
        Vector V2 = null;                                              //Creating Vector2(new object)
        System.out.println("\nEnter dimensions for second Vector? (1 - Yes; 0 - Zero)");
        addnew=in.nextInt();
                    
        if(addnew==1)
        {
            System.out.println("\nEnter dimensions of the second vector[MUST BE LESS THAN OR EQUAL TO 3]: ");
            dim2 =  in.nextInt();
            V2 = new Vector(dim2);
            
            try
            {
                V2.checkdim();
            }
        
            catch(IndexOutOfBounds err)
            {
                System.out.println("\nException occured!\n\n"+err+"\n\nDimension: "+err.r);
                System.out.println("\nEnter dimensions of the second vector[MUST BE LESS THAN OR EQUAL TO 3]: ");
                dim2 =  in.nextInt();
                V2 = new Vector(dim2);
            }
                
            System.out.println("\nEnter for second vector: \n");
            V2.assign();                              //Calling Member function to assign values.
           
        }
        
        while(true)
        {
            
            System.out.println("\n1. Display the vectors.");
            System.out.println("2. Update values of the vector.");
            System.out.println("3. Find the magnitude of the vector.");
            System.out.println("4. Add two vectors.");
            System.out.println("5. Subtract two vectors.");
            System.out.println("6. Find dot(scalar) product of two vectors.");
            System.out.println("7. Find cross(vector) product of two vectors.");
            System.out.println("8. Multiply a scalar with a vector.");
            System.out.println("9. Calculate the polar coordinates of the vector.");
            System.out.println("0. Exit.");
            System.out.println("\nEnter your choice: \n");

            ch = in.nextByte();
            
            switch(ch)
            {
                case 1:
                {
                    System.out.println("\n\nFirst Vector is: ");
                    V1.display();                              //Calling Member function to display values
                    
                    if(V2 == null)
                        System.out.print("\n\nThe second vector hasn't been entered yet!\n");
                    
                    else if(V2 != null)
                    {
                        System.out.println("\n\nSecond vector is: ");
                        V2.display();                              //Calling Member function to display values.
                    }
                        
                    
                }
                break;
                
                case 2:
                {
                    System.out.println("\n\nUpdate Vector1 or Vector2? : ");
                    choice=in.nextByte();
                    if(choice==1)
                        V1.update();                              //Calling Member function to update values.
                    
                    if(choice == 2)
                    {
                        if(V2==null)
                        {
                            System.out.println("\nThe second vector hasn't been entered yet! Enter here!\n");
                            System.out.println("\nEnter dimensions of the second vector[MUST BE LESS THAN OR EQUAL TO 3]: ");
                            dim2 =  in.nextInt();

                            System.out.println("Enter for second vector: \n");
                            V2 = new Vector(dim2);
                            V2.assign();                              //Calling Member function to assign values to new vector.
                        }
                    
                       else if(V2 != null)
                            V2.update();                              //Calling Member function to update values.
                        
                    }
                    
                }
                break;
                
                case 3:
                {
                    System.out.println("\nFind magnitude of Vector1 or Vector2? : ");
                    System.out.println("Enter your choice: ");
                    choice=in.nextByte();
                    
                    if(choice==1)
                        V1.magnitude();                              //Calling Member function calculate magnitude of the vector.
                    
                    else if(choice==2)
                    {
                        if(V2==null)
                        {
                            System.out.println("\nThe second vector hasn't been entered yet! Enter here!\n");
                            System.out.println("\nEnter dimensions of the second vector[MUST BE LESS THAN OR EQUAL TO 3]: ");
                            dim2 =  in.nextInt();

                            System.out.println("Enter for second vector: \n");
                            V2 = new Vector(dim2);
                            V2.assign();                              //Calling Member function to assign values to new vector.
                        }
                        
                        else if(V2 != null)
                        {
                            V2.magnitude();                              //Calling Member function to calculate magnitude of the vector.
                        }
                    }
                   
                    
                }
                break;
                
                case 4:
                {
                    if(V2==null)
                    {
                        System.out.println("\nThe second vector hasn't been entered yet! Enter here!\n");
                        System.out.println("\nEnter dimensions of the second vector[MUST BE LESS THAN OR EQUAL TO 3]: ");
                        dim2 =  in.nextInt();

                        System.out.println("\nEnter for second vector: \n");
                        V2 = new Vector(dim2);
                        V2.assign();                              //Calling Member function to assign values to new vector.
                        System.out.println("\n\n");
                        V1.add(V2);                              //Calling Member function to add two vectors.
                                
                    }
                    
                    else if(V2 != null)
                    {                              //Calling Member function to add two vectors.
                        V1.add(V2);
                    }
                    
                }
                
                break;
                
                case 5:
                {
                    System.out.println("1. V1 - V2.");
                    System.out.println("2. V2 - V1.");
                    System.out.println("Enter your choice: ");
                    choice=in.nextByte();
                    
                    if(V2==null)
                    {
                        System.out.println("\nThe second vector hasn't been entered yet! Enter here!\n");
                        System.out.println("\nEnter dimensions of the second vector[MUST BE LESS THAN OR EQUAL TO 3]: ");
                        dim2 =  in.nextInt();

                        System.out.println("Enter for second vector: \n");
                        V2 = new Vector(dim2);
                        V2.assign();                              //Calling Member function to assign values to new vector.
                    }
                    
                    if(choice==1)
                        V1.subtract(V2);                              //Calling Member function to subtract Vector 2 FROM Vector 1.
                    
                    else if(choice==2)
                    {
                        if(V2 != null)
                        {
                            V2.subtract(V1);                              //Calling Member function to subtract Vector 1 FROM Vector 2.
                        }
                    }
                        
                    else
                            System.out.println("\nEnter valid choice!!");
                    
                }
                
                break;
                
                case 6:
                {
                    if(V2==null)
                    {
                        System.out.println("\nThe second vector hasn't been entered yet! Enter here!\n");
                        System.out.println("\nEnter dimensions of the second vector[MUST BE LESS THAN OR EQUAL TO 3]: ");
                        dim2 =  in.nextInt();

                        System.out.println("Enter for second vector: \n");
                        V2 = new Vector(dim2);
                        V2.assign();                              //Calling Member function to assign values to new vector.
                        
                        V1.scalarprod(V2);                              //Calling Member function to find scalar product.
                    }
                    
                    else if(V2 != null)
                    {
                        V1.scalarprod(V2);                              //Calling Member function to find scalar product.
                    }
                    
                }
                
                break;
                
                case 7:
                {
                    System.out.println("1. V1 x V2.");
                    System.out.println("2. V2 x V1.");
                    System.out.println("Enter your choice: ");
                    choice=in.nextByte();
                    
                    if(V2==null)
                    {
                        System.out.println("\nThe second vector hasn't been entered yet! Enter here!\n");
                        System.out.println("\nEnter dimensions of the second vector[MUST BE LESS THAN OR EQUAL TO 3]: ");
                        dim2 =  in.nextInt();

                        System.out.println("Enter for second vector: \n");
                        V2 = new Vector(dim2);
                        V2.assign();                              //Calling Member function to assign values to new vector.
                    }
                    
                    if(choice==1)
                        V1.vectorprod(V2);                              //Calling Member function to find V1 x V2.
                    
                    else if(choice==2)
                    {
                        if(V2 != null)
                        {
                            V2.vectorprod(V1);                              //Calling Member function to V2 x V1.
                        }
                    }
                        
                    else
                            System.out.println("\nEnter valid choice!!");
               
                }
                
                break;
                case 8:
                {
                    System.out.println("\nEnter the constant: ");
                    constant = in.nextInt();
                    
                    System.out.println("\nMultiply with Vector1 or Vector2? : ");
                    choice = in.nextByte();
                    
                    if(choice==1)
                        V1.multiply(constant);                              //Calling Member function to multiply constant with vector.
                    
                    else if(choice == 2)
                    {
                        if(V2==null)
                        {
                            System.out.println("\nThe second vector hasn't been entered yet! Enter here!\n");
                            System.out.println("\nEnter dimensions of the second vector[MUST BE LESS THAN OR EQUAL TO 3]: ");
                            dim2 =  in.nextInt();

                            System.out.println("Enter for second vector: \n");
                            V2 = new Vector(dim2);
                            V2.assign();                              //Calling Member function to assign values to new vector.

                            V2.multiply(constant);                              //Calling Member function to multiply constant with vector.
                        }

                        else if(V2 != null)
                        {
                            V2.multiply(constant);                              //Calling Member function to multiply constant with vector.
                        }

                    }
                    
                    else
                        System.out.println("\nEnter valid choice!!");
                    
                }
                
                break;
                
               case 9:
                {
                    System.out.println("\nFind Polar Coordinates of Vector1 or Vector2? : ");
                    choice = in.nextByte();
                    
                    if(choice==1)
                        V1.polar();                              //Calling Member function to calculate polar coordinates of vector.
                    
                    else if(choice == 2)
                    {
                        if(V2==null)
                        {
                            System.out.println("\nThe second vector hasn't been entered yet! Enter here!\n");
                            System.out.println("\nEnter dimensions of the second vector[MUST BE LESS THAN OR EQUAL TO 3]: ");
                            dim2 =  in.nextInt();

                            System.out.println("Enter for second vector: \n");
                            V2 = new Vector(dim2);
                            V2.assign();                              //Calling Member function to assign values to new vector.

                            V2.polar();                              //Calling Member function to calculate polar coordinates of vector.
                        }

                        else if(V2 != null)
                        {
                            V2.polar();                              //Calling Member function to calculate polar coordinate of vector.
                        }

                    
                    }
                    
                    else
                        System.out.println("\nEnter valid choice!!");
                }
                break;
                
                case 0: return;                                             //To exit the loop.
                
                default: System.out.println("Enter valid choice!");
            }
        }
        
    }
    
}

