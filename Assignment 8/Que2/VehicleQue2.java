
    package vehicle.que2;

    import java.util.*;



    public class VehicleQue2 {


        public static void main(String[] args) {

            int ch;
            boolean b=true;
            LinkedList<Vehicle> Vehicle = new LinkedList<Vehicle>();

            while(true)
            {

                System.out.println("1. Add a vehicle");
                System.out.println("2. Displat the list of vehicles");
                System.out.println("3. Update vehicle information");
                System.out.println("4. Delete a vehicle");
                System.out.println("5. Sell a vehicle");
                System.out.println("6. Give a vehicle on rent");
                System.out.println("0. Exit");
                System.out.println("\nPlease enter a choice");
                Scanner in = new Scanner (System.in);
                ch=in.nextInt();

                switch(ch)
                {
                    case 1 :

                        System.out.println("Which type of vehicle you would like to add?");
                        System.out.println("1. Two wheeler");
                        System.out.println("2. Four wheeler");
                        System.out.println("\nPlease enter a choice");
                        ch=in.nextInt();

                        if(ch==1)
                        {
                            Vehicle v;
                            System.out.println("Please enter the size of the engine(in Litres)");
                            float size=in.nextFloat();
                            v= new TwoWheeler(size);
                            v.type="Two Wheeler";
                            v.readInfo();
                            Vehicle.add(v);
                        }
                        if(ch==2)
                        {
                            FourWheeler v; 
                            System.out.println("Which type of Four Wheeler you would like to add?");
                            System.out.println("1. Private car");
                            System.out.println("2. Commercial car");
                            System.out.println("\nPlease enter a choice");
                            ch=in.nextInt();

                            if(ch==1)
                            {
                                System.out.println("Please enter the size of the engine(in Litres)");
                                float size=in.nextFloat();
                                v=new PrivateCar(size);
                                v.type="Four Wheeler";
                                v.subtype="Private Car";
                                v.readInfo();

                                Vehicle.add(v);
                            }
                            if(ch==2)
                            {
                                System.out.println("Please enter the size of the engine(in Litres)");
                                float size=in.nextFloat();
                                v=new CommercialCar(size);
                                v.type="Four Wheeler";
                                v.subtype="Commercial Car";
                                v.readInfo();

                                Vehicle.add(v);

                            }
                        }
                        break;
                    case 2:
                        if(Vehicle.isEmpty()==true)
                            System.out.println("The list is empty");
                        else
                        {
                            for(int i=0;i<Vehicle.size();i++)
                            {
                                System.out.println("Position: "+ i);
                                System.out.println("Type: "+ Vehicle.get(i).type);
                                System.out.println("SubType: "+ Vehicle.get(i).subtype);
                                Vehicle.get(i).displayinfo();
                                System.out.println("\n\n");


                            }
                        }
                        break;
                    case 4:
                        if(Vehicle.isEmpty()==true)
                            System.out.println("The list is empty");
                        else
                        {
                            for(int i=0;i<Vehicle.size();i++)
                            {
                                System.out.println("Position: "+ i);
                                System.out.println("Type: "+ Vehicle.get(i).type);
                                System.out.println("SubType: "+ Vehicle.get(i).subtype);
                                Vehicle.get(i).displayinfo();
                                System.out.println("\n\n");


                            }
                            System.out.println("Please enter the position of the object you would like to delete");
                            int pos = in.nextInt();
                            Vehicle.remove(pos);
                            System.out.println("Object successfully removed");
                        }
                        break;
                    case 3:
                        if(Vehicle.isEmpty()==true)
                            System.out.println("The list is empty");
                        else
                        {
                            for(int i=0;i<Vehicle.size();i++)
                            {
                                System.out.println("Position: "+ i);
                                System.out.println("Type: "+ Vehicle.get(i).type);
                                System.out.println("SubType: "+ Vehicle.get(i).subtype);
                                Vehicle.get(i).displayinfo();
                                System.out.println("\n\n");


                            }
                            System.out.println("Please enter the position of the object you would like to update");
                            int pos = in.nextInt();
                            Vehicle.get(pos).readInfo();
                            System.out.println("Information updated successfully");
                }
                        break;
                    case 5:
                        if(Vehicle.isEmpty()==true)
                            System.out.println("The list is empty");
                        else
                        {
                            for(int i=0;i<Vehicle.size();i++)
                            {
                                System.out.println("Position: "+ i);
                                System.out.println("Type: "+ Vehicle.get(i).type);
                                System.out.println("SubType: "+ Vehicle.get(i).subtype);
                                Vehicle.get(i).displayinfo();
                                System.out.println("\n\n");


                            }
                        }


                        System.out.println("Please enter the chassis number of the vehicle you want to sell");
                        b=true;
                        while(b)
                        {String chassis=in.next();
                        for(int i =0;i<Vehicle.size();i++)
                        {
                            if(chassis == null ? Vehicle.get(i).chassis == null : chassis.equals(Vehicle.get(i).chassis))
                            {
                                Vehicle.get(i).Sell();
                                b=false;
                                break;

                            }
                            else
                            {
                                System.out.println("Please enter correct chassis number");
                                break;
                            }
                        }
                        }
                        break;
                    case 6 :
                        int cnt=0;
                       if(Vehicle.isEmpty()==true)
                            System.out.println("The list is empty");
                        else
                        {
                            for(int i=0;i<Vehicle.size();i++)
                            {
                                if(Vehicle.get(i).type=="Two Wheeler"||Vehicle.get(i).subtype=="Commercial Car")
                                {
                                    System.out.println("Position: "+ i);
                                    System.out.println("Type: "+ Vehicle.get(i).type);
                                    System.out.println("SubType: "+ Vehicle.get(i).subtype);
                                    Vehicle.get(i).displayinfo();
                                    System.out.println("\n\n");
                                    cnt++;
                                }



                            }
                            if(cnt==0)
                            {
                                System.out.println("There is no vehicle available for giving on rent");
                                break;
                            }
                        }


                        System.out.println("Please enter the chassis number of the vehicle you want to give on rent");
                        b=true;
                        while(b)
                        {String chassis=in.next();
                        for(int i =0;i<Vehicle.size();i++)
                        {
                            if(chassis == null ? Vehicle.get(i).chassis == null : chassis.equals(Vehicle.get(i).chassis))
                            {
                                if(Vehicle.get(i).type=="Two Wheeler")                         
                                {((TwoWheeler)(Vehicle.get(i))).Rent();
                                b=false;
                                break;
                                }
                                if(Vehicle.get(i).subtype=="Commercial Car")                         
                                {((CommercialCar)(Vehicle.get(i))).Rent();
                                b=false;
                                break;
                                }


                            }
                            else
                            {
                                System.out.println("Please enter correct chassis number");
                                break;
                            }
                        }
                        }
                        break;


                    case 0:
                        return;
            }
        }

    }
    }