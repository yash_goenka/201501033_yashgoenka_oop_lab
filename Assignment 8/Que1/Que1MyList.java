package que1.Main;

import java.util.Scanner;


public class Que1MyList {

   
    public static void main(String[] args) {
  
        int ch,length;
        
        System.out.println("Please enter the length of the array");
        Scanner in = new Scanner(System.in);
        length=in.nextInt();
        
        //Creating instance of class 'MyList' 
        
        MyList list;
        list = new MyList(length);
        list.read();
        
       
       
        
        
        while(true)
        {
            //Printing the menu
            
            System.out.println("1. Search a number");
            System.out.println("2. Sort the list");
            System.out.println("3. Display the list");
            System.out.println("0. Exit");
            System.out.println("\n Please enter a choice");
            ch=in.nextInt();
            
            if(ch==1)
            {
                System.out.println("Please enter the number to search");
                int num = in.nextInt();
                
                //Calling function 'search' for searching 
                list.Search(num);
            }
            if(ch==2)
            {
                System.out.println("1. Sort in ascending order");
                System.out.println("2. Sort in descending order");
                System.out.println("\n Please enter a choice");
                ch=in.nextInt();
                
                if(ch==1)
                {
                    
                    //Calling function 'Sort' for sorting the list in ascending order
                    list.Sort(ch);
                    System.out.println("The sorted list is: ");
                    list.display(list);
                }
                if(ch==2)
                {
                    
                    //Calling function 'Sort' for sorting the list in descending order
                    list.Sort(ch);
                    System.out.println("The sorted list is: ");
                    list.display(list);
                }
            }
            if(ch==3)
            {
                //Calling funtion 'display' for displaying the list
                list.display(list);
                
            }
                
            if(ch==0)
                
                //termination of the application
                return;
            
        }
    }
    
}
