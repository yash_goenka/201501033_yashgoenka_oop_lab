
package que1.Main;


import java.util.Scanner;

//Importing package 'que1.Interface'
import que1.Interface.*;


public class MyList implements Searchable,Sortable {
    
    
    //Creating reference of the list
    int [] List;  
    int length;
    Scanner in = new Scanner(System.in);
     MyList(int length)
    {
        this.length=length;
        
        //Creating instance of the list
        List = new int[this.length];
    }
    
     //Creating copy constructor
     MyList(MyList l)
    {
        
        l.length=length;
        l.List=new int[length];
        for(int i =0;i<length;i++)
        {
            l.List[i]=List[i];
        }
    }

   
    //Overiding 'Search' method declared in que1.Interface
    public void Search(int m) {
        int cnt=0;
        
        //Logic for searching the number
        for(int i=0 ;i<List.length;i++)
        {
            if(m==List[i])
            {
                cnt++;
            }
        }
        
        if(cnt==0)
        {
            System.out.println("The entered number was not found in the list");
        }
        else
            System.out.println(m +" occurs " + cnt + " time(s) in the list");
    }

    //Overiding 'Sort' function declared in que1.Interface
    public void Sort(int ch) {
        int temp;
        
        if(ch==1)
        {
            for(int i=0;i<List.length;i++)
            {
                for (int j=i+1;j<List.length;j++)
                {
                    if(List[i]>List[j])
                    {
                        temp=List[i];
                        List[i]=List[j];
                        List[j]=temp;
                        
                    }
                }
            }
        }
        if(ch==2)
        {
            for(int i=0;i<List.length;i++)
            {
                for (int j=i+1;j<List.length;j++)
                {
                    if(List[i]<List[j])
                    {
                        temp=List[i];
                        List[i]=List[j];
                        List[j]=temp;
                        
                    }
                }
            }
        }
    }
    
    //Reading the values of the elements of the list from the user
    public void read()
    {
        for(int i=0;i<List.length;i++)
        {
            System.out.println("Enter element number "+(i+1));
            List[i]=in.nextInt();
        }
    }
    
    //Displaying the list
    public void display(MyList l)
    {
        
        
        
        for(int i=0;i<l.length;i++)
        {
            System.out.println(l.List[i]);
        }
    }
    
}



   

